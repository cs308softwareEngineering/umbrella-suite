/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.model;

import java.lang.reflect.Field;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author beyzabozbey
 */
public class CustomersTest {
    
    public CustomersTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getCid method, of class Customers.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testGetCid() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("getCid");
        final Customers instance = new Customers();
        final Field field = instance.getClass().getDeclaredField("cid");
        field.setAccessible(true);
        field.set(instance, 5);
        final int result = instance.getCid();
        try
        {
            assertEquals(result, 5);
        }catch(AssertionError e)
        {
            fail("Customers.getCid() fails.");
        }
        System.out.println("umbrella_suite.model.CustomersTest.testGetCid() passed.");
        
    }

    /**
     * Test of setCid method, of class Customers.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testSetCid() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("setCid");
        final Customers instance = new Customers();
        instance.setCid(5);
        final Field field = instance.getClass().getDeclaredField("cid");
        field.setAccessible(true);
        try
        {
            assertEquals(field.get(instance), 5);
        }catch(AssertionError e)
        {
            fail("Customers.setCid() fails.");
        }
        System.out.println("umbrella_suite.model.CustomersTest.testSetCid() passed.");
        
    }

    /**
     * Test of getEmail method, of class Customers.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testGetEmail() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("getEmail");
        final Customers instance = new Customers();
        final Field field = instance.getClass().getDeclaredField("email");
        field.setAccessible(true);
        field.set(instance, "abc@abc.com");
        final String result = instance.getEmail();
        try
        {
            assertEquals(result, "abc@abc.com");
        }catch(AssertionError e)
        {
            fail("Customers.getEmail() fails.");
        }
        System.out.println("umbrella_suite.model.CustomersTest.testGetEmail() passed.");
        
    }

    /**
     * Test of setEmail method, of class Customers.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testSetEmail() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("setEmail");
        final Customers instance = new Customers();
        instance.setEmail("abc@abc.com");
        final Field field = instance.getClass().getDeclaredField("email");
        field.setAccessible(true);
        try
        {
            assertEquals(field.get(instance), "abc@abc.com");
        }catch(AssertionError e)
        {
            fail("Customers.setEmail() fails.");
        }
        System.out.println("umbrella_suite.model.CustomersTest.testSetEmail() passed.");
        
    }

    /**
     * Test of getPwd method, of class Customers.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testGetPwd() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("getPwd");
        final Customers instance = new Customers();
        final Field field = instance.getClass().getDeclaredField("pwd");
        field.setAccessible(true);
        field.set(instance, "123456");
        final String result = instance.getPwd();
        try
        {
            assertEquals(result, "123456");
        }catch(AssertionError e)
        {
            fail("Customers.getPwd() fails.");
        }
        System.out.println("umbrella_suite.model.CustomersTest.testGetPwd() passed.");
        
    }

    /**
     * Test of setPwd method, of class Customers.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testSetPwd() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("setPwd");
        final Customers instance = new Customers();
        instance.setPwd("123456");
        final Field field = instance.getClass().getDeclaredField("pwd");
        field.setAccessible(true);
        try
        {
            assertEquals(field.get(instance), "123456");
        }catch(AssertionError e)
        {
            fail("Customers.setPwd() fails.");
        }
        System.out.println("umbrella_suite.model.CustomersTest.testSetPwd() passed.");
        
    }

    /**
     * Test of getSalt method, of class Customers.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testGetSalt() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("getSalt");
        final Customers instance = new Customers();
        final Field field = instance.getClass().getDeclaredField("salt");
        field.setAccessible(true);
        field.set(instance, "abcdef");
        final String result = instance.getSalt();
        try
        {
            assertEquals(result, "abcdef");
        }catch(AssertionError e)
        {
            fail("Customers.getSalt() fails.");
        }
        System.out.println("umbrella_suite.model.CustomersTest.testGetSalt() passed.");
        
    }

    /**
     * Test of setSalt method, of class Customers.
     * @throws java.lang.IllegalAccessException
     * @throws java.lang.NoSuchFieldException
     */
    @Test
    public void testSetSalt() throws IllegalAccessException, NoSuchFieldException {
        System.out.println("setSalt");
        final Customers instance = new Customers();
        instance.setSalt("abcdef");
        final Field field = instance.getClass().getDeclaredField("salt");
        field.setAccessible(true);
        try
        {
            assertEquals(field.get(instance), "abcdef");
        }catch(AssertionError e)
        {
            fail("Customers.setSalt() failed.");
        }
        System.out.println("umbrella_suite.model.CustomersTest.testSetSalt() passed."); 
    }

    /**
     * Test of getFirstName method, of class Customers.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testGetFirstName() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("getFirstName");
        final Customers instance = new Customers();
        final Field field = instance.getClass().getDeclaredField("firstName");
        field.setAccessible(true);
        field.set(instance, "John");
        final String result = instance.getFirstName();
        try
        {
            assertEquals(result, "John");
        }catch(AssertionError e)
        {
            fail("Customers.getFirstName() fails.");
        }
        System.out.println("umbrella_suite.model.CustomersTest.testGetFirstName() passed.");
    }

    /**
     * Test of setFirstName method, of class Customers.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testSetFirstName() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("setFirstName");
        final Customers instance = new Customers();
        instance.setFirstName("John");
        final Field field = instance.getClass().getDeclaredField("firstName");
        field.setAccessible(true);
        try
        {
            assertEquals(field.get(instance), "John");
        }catch(AssertionError e)
        {
            fail("Customers.setFirstName() fails.");
        }
        System.out.println("umbrella_suite.model.CustomersTest.testSetFirstName() passed.");
    }

    /**
     * Test of getLastName method, of class Customers.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testGetLastName() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("getLastName");
        final Customers instance = new Customers();
        final Field field = instance.getClass().getDeclaredField("lastName");
        field.setAccessible(true);
        field.set(instance, "Black");
        final String result = instance.getLastName();
        try
        {
            assertEquals(result, "Black");
        }catch(AssertionError e)
        {
            fail("Customers.getLastName() fails.");
        }
        System.out.println("umbrella_suite.model.CustomersTest.testGetLastName() passed.");
    }

    /**
     * Test of setLastName method, of class Customers.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testSetLastName() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("setLastName");
        final Customers instance = new Customers();
        instance.setLastName("Black");
        final Field field = instance.getClass().getDeclaredField("lastName");
        field.setAccessible(true);
        try
        {
            assertEquals(field.get(instance), "Black");
        }catch(AssertionError e)
        {
            fail("Customers.setLastName() fails.");
        }
        System.out.println("umbrella_suite.model.CustomersTest.testSetLastName() passed.");
    }

    /**
     * Test of getAddress method, of class Customers.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testGetAddress() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("getAddress");
        final Customers instance = new Customers();
        final Field field = instance.getClass().getDeclaredField("address");
        field.setAccessible(true);
        field.set(instance, "Susame Street");
        final String result = instance.getAddress();
        try
        {
            assertEquals(result, "Susame Street");
        }catch(AssertionError e)
        {
            fail("Customers.getAddress() fails.");
        }
        System.out.println("umbrella_suite.model.CustomersTest.testGetAddress() passed.");
    }

    /**
     * Test of setAddress method, of class Customers.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testSetAddress() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("setAddress");
        final Customers instance = new Customers();
        instance.setAddress("Susame Street");
        final Field field = instance.getClass().getDeclaredField("address");
        field.setAccessible(true);
        try
        {
            assertEquals(field.get(instance), "Susame Street");
        }catch(AssertionError e)
        {
            fail("Customers.setAddress() fails.");
        }
        System.out.println("umbrella_suite.model.CustomersTest.testSetAddress() passed.");
    }

    /**
     * Test of hashCode method, of class Customers.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Customers instance = new Customers();
        int expResult = 0;
        instance.setCid(null);
        int result = instance.hashCode();
        try
        {
            assertEquals(expResult, result);
        }catch(AssertionError e)
        {
            fail("Authenticated.hashCode() null cid fails.");
        }
        Integer cid = 5;
        instance.setCid(cid);
        expResult = cid.hashCode();
        result = instance.hashCode();
        try
        {
            assertEquals(expResult, result);
        }catch(AssertionError e)
        {
            fail("Authenticated.hashCode() fails.");
        }
        System.out.println("umbrella_suite.model.CustomersTest.testHashCode() passed.");
    }

    /**
     * Test of equals method, of class Customers.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Customers instance = new Customers();
        boolean result;
        instance.setCid(5);
        instance.setFirstName("John");
        instance.setLastName("Black");
        instance.setAddress("Susame Street");
        instance.setEmail("john@black.com");
        instance.setPwd("123456");
        instance.setSalt("abcdef");
        Customers instance2 = new Customers();
        instance2.setCid(5);
        instance2.setFirstName("John");
        instance2.setLastName("Black");
        instance2.setAddress("Susame Street");
        instance2.setEmail("john@black.com");
        instance2.setPwd("123456");
        instance2.setSalt("abcdef");
        result = instance.equals(instance2);
        try
        {
            assertEquals(true, result);
        }catch(AssertionError e)
        {
            fail("Customers.equals() fails.");
        }
        instance2.setCid(10);
        instance2.setFirstName("John");
        instance2.setLastName("Black");
        instance2.setAddress("Susame Street");
        instance2.setEmail("johnn@black.com");
        instance2.setPwd("123456");
        instance2.setSalt("abcdef");
        result = instance.equals(instance2);
        try
        {
            assertEquals(false, result);
        }catch(AssertionError e)
        {
            fail("Customers.equals() fails.");
        }
        System.out.println("umbrella_suite.model.CustomersTest.testEquals() passed.");
    }

    /**
     * Test of toString method, of class Customers.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Customers instance = new Customers();
        instance.setCid(5);
        String expResult = "umbrella_suite.model.Customers[ cid=5 ]";
        String result = instance.toString();
        try
        {
            assertEquals(expResult, result);
        }catch(AssertionError e)
        {
            fail("Customers.toString() fails.");
        }
        System.out.println("umbrella_suite.model.CustomersTest.testToString() passed.");
    }
    
}
