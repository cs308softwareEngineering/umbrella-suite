/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.model;

import java.lang.reflect.Field;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author beyzabozbey
 */
public class AuthenticatedTest {
    
    public AuthenticatedTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getToken method, of class Authenticated.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testGetToken() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("getToken");
        final Authenticated instance = new Authenticated();
        final Field field = instance.getClass().getDeclaredField("token");
        field.setAccessible(true);
        field.set(instance, "value");
        final String result = instance.getToken();
        try
        {
            assertEquals(result, "value");
        }catch(AssertionError e)
        {
            fail("Authenticated.getToken() failed.");
        }
        System.out.println("umbrella_suite.model.AuthenticatedTest.testGetToken() passed.");
    }

    /**
     * Test of setToken method, of class Authenticated.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testSetToken() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("setToken");
        final Authenticated instance = new Authenticated();
        instance.setToken("foo");
        final Field field = instance.getClass().getDeclaredField("token");
        field.setAccessible(true);
        try
        {
            assertEquals(field.get(instance), "foo");
        }catch(AssertionError e)
        {
            fail("Authenticated.setToken() failed.");
        }
        System.out.println("umbrella_suite.model.AuthenticatedTest.testSetToken() passed.");
    }

    /**
     * Test of getEmail method, of class Authenticated.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testGetEmail() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("getEmail");
        final Authenticated instance = new Authenticated();
        final Field field = instance.getClass().getDeclaredField("email");
        field.setAccessible(true);
        field.set(instance, "abc@abc.com");
        final String result = instance.getEmail();
        try
        {
            assertEquals(result, "abc@abc.com");
        }catch(AssertionError e)
        {
            fail("Authenticated.getEmail() failed.");
        }
        System.out.println("umbrella_suite.model.AuthenticatedTest.testGetEmail() passed.");
    }

    /**
     * Test of setEmail method, of class Authenticated.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testSetEmail() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("setEmail");
        final Authenticated instance = new Authenticated();
        instance.setEmail("abc@abc.com");
        final Field field = instance.getClass().getDeclaredField("email");
        field.setAccessible(true);
        try
        {
            assertEquals(field.get(instance), "abc@abc.com");
        }catch(AssertionError e)
        {
            fail("Authenticated.setEmail() failed.");
        }
        System.out.println("umbrella_suite.model.AuthenticatedTest.testSetEmail() passed.");
    }

    /**
     * Test of hashCode method, of class Authenticated.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Authenticated instance = new Authenticated();
        int expResult = 0;
        instance.setToken(null);
        int result = instance.hashCode();
        try
        {
            assertEquals(expResult, result);
        }catch(AssertionError e)
        {
            fail("Authenticated.hashCode() null token fails.");
        }
        String token = "12345";
        instance.setToken(token);
        expResult = token.hashCode();
        result = instance.hashCode();
        try
        {
            assertEquals(expResult, result);
        }catch(AssertionError e)
        {
            fail("Authenticated.hashCode() fails.");
        }
        System.out.println("umbrella_suite.model.AuthenticatedTest.testHashCode() passed.");
    }

    /**
     * Test of equals method, of class Authenticated.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Authenticated instance = new Authenticated();
        boolean result;
        instance.setToken("12345");
        instance.setEmail("abc@abc.com");
        Authenticated instance2 = new Authenticated();
        instance2.setToken("12345");
        instance2.setEmail("abc@abc.com");
        result = instance.equals(instance2);
        try
        {
            assertEquals(true, result);
        }catch(AssertionError e)
        {
            fail("Authenticated.equals() fails.");
        }
        instance2.setToken("54321");
        instance2.setEmail("def@def.com");
        result = instance.equals(instance2);
        try
        {
            assertEquals(false, result);
        }catch(AssertionError e)
        {
            fail("Authenticated.equals() fails.");
        }
        System.out.println("umbrella_suite.model.AuthenticatedTest.testEquals() passed.");
    }

    /**
     * Test of toString method, of class Authenticated.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Authenticated instance = new Authenticated();
        instance.setToken("12345");
        String expResult = "umbrella_suite.model.Authenticated[ token=12345 ]";
        String result = instance.toString();
        try
        {
            assertEquals(expResult, result);
        }catch(AssertionError e)
        {
            fail("Authenticated.toString() fails.");
        }
        System.out.println("umbrella_suite.model.AuthenticatedTest.testToString() passed.");
    }
    
}
