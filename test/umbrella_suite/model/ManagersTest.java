/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.model;

import java.lang.reflect.Field;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author beyzabozbey
 */
public class ManagersTest {
    
    public ManagersTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getMid method, of class Managers.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testGetMid() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("getMid");
        final Managers instance = new Managers();
        final Field field = instance.getClass().getDeclaredField("mid");
        field.setAccessible(true);
        field.set(instance, 5);
        final int result = instance.getMid();
        try
        {
            assertEquals(5, result);
        }catch(AssertionError e)
        {
            fail("Managers.getMid() failed.");
        }
        System.out.println("umbrella_suite.model.ManagersTest.testGetMid() passed.");
    }

    /**
     * Test of setMid method, of class Managers.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testSetMid() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("setMid");
        final Managers instance = new Managers();
        instance.setMid(5);
        final Field field = instance.getClass().getDeclaredField("mid");
        field.setAccessible(true);
        try
        {
            assertEquals(5, field.get(instance));
        }catch(AssertionError e)
        {
            fail("Managers.setMid() failed.");
        }
        System.out.println("umbrella_suite.model.ManagersTest.testSetMid() passed.");
    }

    /**
     * Test of getEmail method, of class Managers.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testGetEmail() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("getEmail");
        final Managers instance = new Managers();
        final Field field = instance.getClass().getDeclaredField("email");
        field.setAccessible(true);
        field.set(instance, "abc@def.com");
        final String result = instance.getEmail();
        try
        {
            assertEquals("abc@def.com", result);
        }catch(AssertionError e)
        {
            fail("Managers.getEmail() failed.");
        }
        System.out.println("umbrella_suite.model.ManagersTest.testGetEmail() passed.");
    }

    /**
     * Test of setEmail method, of class Managers.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testSetEmail() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("setEmail");
        final Managers instance = new Managers();
        instance.setEmail("abc@def.com");
        final Field field = instance.getClass().getDeclaredField("email");
        field.setAccessible(true);
        try
        {
            assertEquals("abc@def.com", field.get(instance));
        }catch(AssertionError e)
        {
            fail("Managers.setEmail() failed.");
        }
        System.out.println("umbrella_suite.model.ManagersTest.testSetEmail() passed.");
    }

    /**
     * Test of getPwd method, of class Managers.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testGetPwd() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("getPwd");
        final Managers instance = new Managers();
        final Field field = instance.getClass().getDeclaredField("pwd");
        field.setAccessible(true);
        field.set(instance, "12345678");
        final String result = instance.getPwd();
        try
        {
            assertEquals("12345678", result);
        }catch(AssertionError e)
        {
            fail("Managers.getPwd() failed.");
        }
        System.out.println("umbrella_suite.model.ManagersTest.testGetPwd() passed.");
    }

    /**
     * Test of setPwd method, of class Managers.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testSetPwd() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("setPwd");
        final Managers instance = new Managers();
        instance.setPwd("12345678");
        final Field field = instance.getClass().getDeclaredField("pwd");
        field.setAccessible(true);
        try
        {
            assertEquals("12345678", field.get(instance));
        }catch(AssertionError e)
        {
            fail("Managers.setPwd() failed.");
        }
        System.out.println("umbrella_suite.model.ManagersTest.testSetPwd() passed.");
    }

    /**
     * Test of getSalt method, of class Managers.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testGetSalt() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("getSalt");
        final Managers instance = new Managers();
        final Field field = instance.getClass().getDeclaredField("salt");
        field.setAccessible(true);
        field.set(instance, "abcdef");
        final String result = instance.getSalt();
        try
        {
            assertEquals(result, "abcdef");
        }catch(AssertionError e)
        {
            fail("Managers.getSalt() fails.");
        }
        System.out.println("umbrella_suite.model.ManagersTest.testGetSalt() passed.");
    }

    /**
     * Test of setSalt method, of class Managers.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testSetSalt() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("setSalt");
        final Managers instance = new Managers();
        instance.setSalt("abcdef");
        final Field field = instance.getClass().getDeclaredField("salt");
        field.setAccessible(true);
        try
        {
            assertEquals(field.get(instance), "abcdef");
        }catch(AssertionError e)
        {
            fail("Managers.setSalt() failed.");
        }
        System.out.println("umbrella_suite.model.ManagersTest.testSetSalt() passed.");
    }

    /**
     * Test of getFirstName method, of class Managers.
     */
    @Test
    public void testGetFirstName() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("getFirstName");
        final Managers instance = new Managers();
        final Field field = instance.getClass().getDeclaredField("firstName");
        field.setAccessible(true);
        field.set(instance, "john");
        final String result = instance.getFirstName();
        try
        {
            assertEquals("john", result);
        }catch(AssertionError e)
        {
            fail("Managers.getFirstName() failed.");
        }
        System.out.println("umbrella_suite.model.ManagersTest.testGetFirstName() passed.");
    }

    /**
     * Test of setFirstName method, of class Managers.
     */
    @Test
    public void testSetFirstName() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("setFirstName");
        final Managers instance = new Managers();
        instance.setFirstName("john");
        final Field field = instance.getClass().getDeclaredField("firstName");
        field.setAccessible(true);
        try
        {
            assertEquals("john", field.get(instance));
        }catch(AssertionError e)
        {
            fail("Managers.setFirstName() failed.");
        }
        System.out.println("umbrella_suite.model.ManagersTest.testSetFirstName() passed.");
    }

    /**
     * Test of getLastName method, of class Managers.
     */
    @Test
    public void testGetLastName() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("getLastName");
        final Managers instance = new Managers();
        final Field field = instance.getClass().getDeclaredField("lastName");
        field.setAccessible(true);
        field.set(instance, "locke");
        final String result = instance.getLastName();
        try
        {
            assertEquals("locke", result);
        }catch(AssertionError e)
        {
            fail("Managers.getLastName() failed.");
        }
        System.out.println("umbrella_suite.model.ManagersTest.testGetLastName() passed.");
    }

    /**
     * Test of setLastName method, of class Managers.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testSetLastName() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("setLastName");
        final Managers instance = new Managers();
        instance.setLastName("locke");
        final Field field = instance.getClass().getDeclaredField("lastName");
        field.setAccessible(true);
        try
        {
            assertEquals("locke", field.get(instance));
        }catch(AssertionError e)
        {
            fail("Managers.setLastName() failed.");
        }
        System.out.println("umbrella_suite.model.ManagersTest.testSetLastName() passed.");
    }

    /**
     * Test of getType method, of class Managers.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testGetType() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("getType");
        final Managers instance = new Managers();
        final Field field = instance.getClass().getDeclaredField("type");
        field.setAccessible(true);
        field.set(instance, "value");
        final String result = instance.getType();
        try
        {
            assertEquals("value", result);
        }catch(AssertionError e)
        {
            fail("Managers.getType() failed.");
        }
        System.out.println("umbrella_suite.model.ManagersTest.testGetType() passed.");
    }

    /**
     * Test of setType method, of class Managers.
     * @throws java.lang.NoSuchFieldException
     * @throws java.lang.IllegalAccessException
     */
    @Test
    public void testSetType() throws NoSuchFieldException, IllegalAccessException {
        System.out.println("setType");
        final Managers instance = new Managers();
        instance.setType("value");
        final Field field = instance.getClass().getDeclaredField("type");
        field.setAccessible(true);
        try
        {
            assertEquals("value", field.get(instance));
        }catch(AssertionError e)
        {
            fail("Managers.setType() failed.");
        }
        System.out.println("umbrella_suite.model.ManagersTest.testSetType() passed.");
    }

    /**
     * Test of hashCode method, of class Managers.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Managers instance = new Managers();
        int expResult = 0;
        instance.setMid(null);
        int result = instance.hashCode();
        try
        {
            assertEquals(expResult, result);
        }catch(AssertionError e)
        {
            fail("Managers.hashCode() null description fails.");
        }
        Integer mid = 5;
        instance.setMid(mid);
        expResult = mid.hashCode();
        result = instance.hashCode();
        try
        {
            assertEquals(expResult, result);
        }catch(AssertionError e)
        {
            fail("Managers.hashCode() fails.");
        }
        System.out.println("umbrella_suite.model.ManagersTest.testHashCode() passed.");
    }

    /**
     * Test of equals method, of class Managers.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Managers instance = new Managers();
        boolean result;
        instance.setMid(5);
        instance.setEmail("abc@def.com");
        instance.setPwd("12345678");
        instance.setFirstName("john");
        instance.setLastName("locke");
        instance.setType("value");
        instance.setSalt("abcdef");
        Managers instance2 = new Managers();
        instance2.setMid(5);
        instance2.setEmail("abc@def.com");
        instance2.setPwd("12345678");
        instance2.setFirstName("john");
        instance2.setLastName("locke");
        instance2.setType("value");
        instance2.setSalt("abcdef");
        result = instance.equals(instance2);
        try
        {
            assertEquals(true, result);
        }catch(AssertionError e)
        {
            fail("Managers.equals() equal failed.");
        }
        instance2.setMid(20);
        instance2.setEmail("xyz@zyx.com");
        result = instance.equals(instance2);
        try
        {
            assertEquals(false, result);
        }catch(AssertionError e)
        {
            fail("Managers.equals() not equal failed.");
        }
        System.out.println("umbrella_suite.model.ManagersTest.testEquals() passed.");
    }

    /**
     * Test of toString method, of class Managers.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Managers instance = new Managers();
        instance.setMid(5);
        String expResult = "umbrella_suite.model.Managers[ mid=5 ]";
        String result = instance.toString();
        try
        {
            assertEquals(expResult, result);
        }catch(AssertionError e)
        {
            fail("Invoices.toString() failed.");
        }
        System.out.println("umbrella_suite.model.ManagersTest.testToString() passed.");
    }
    
}
