/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

//import static org.mockito.Mockito.*;

/**
 *
 * @author beyzabozbey
 */
@RunWith(MockitoJUnitRunner.class)
public class MainControllerTest extends Mockito{
    
    /*private MainControllerTest servlet = new MainControllerTest();
    private HttpServletRequest request;
    private HttpServletResponse response;
    @Mock private HttpSession session;
    @Mock private Shoppingcart shoppingCart;
    @Mock private BackendService backendService;
    @Mock private Products someProduct;*/
    
    
    public MainControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        /*servlet.setBackendService(backendService);
        when(request.getSession()).thenReturn(session);
        when(session.getAttribute("shoppingCart")).thenReturn(shoppingCart);*/
        
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of processRequest method, of class MainController.
     */
    
    @Test
    public void testProcessRequest() throws Exception {
        System.out.println("processRequest");
        
        HttpServletRequest request = mock(HttpServletRequest.class);       
        HttpServletResponse response = mock(HttpServletResponse.class);    
        when(request.getParameter("action")).thenReturn("me");
        
        MainController instance = new MainController();
        instance.processRequest(request, response);

        verify(request, atLeast(1)).getParameter("action"); 
        System.out.println("umbrella_suite.controller.MainControllerTest.testProcessRequest() getParameter() passed.");    
    }

    /**
     * Test of doGet method, of class MainController.
     */
    @Test
    public void testDoGet() throws Exception {
        System.out.println("doGet");
        
        HttpServletRequest request = mock(HttpServletRequest.class);       
        HttpServletResponse response = mock(HttpServletResponse.class);    
        
        when(request.getParameter("action")).thenReturn("me");
        MainController instance = new MainController();
        instance.doGet(request, response);

        verify(request, atLeast(1)).getParameter("action"); 
        System.out.println("umbrella_suite.controller.MainControllerTest.testDoGet() passed.");
    }

    /**
     * Test of doPost method, of class MainController.
     */
    @Test
    public void testDoPost() throws Exception {
        System.out.println("doPost");
        HttpServletRequest request = mock(HttpServletRequest.class);       
        HttpServletResponse response = mock(HttpServletResponse.class);    
        
        when(request.getParameter("action")).thenReturn("me");
        MainController instance = new MainController();
        instance.doPost(request, response);

        verify(request, atLeast(1)).getParameter("action"); 
        System.out.println("umbrella_suite.controller.MainControllerTest.testDoPost() passed.");
    }
    
}
