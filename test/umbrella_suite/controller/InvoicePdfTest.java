/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.controller;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mbenlioglu
 */
public class InvoicePdfTest {
    
    public InvoicePdfTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of createPdf method, of class InvoicePdf.
     */
    @Test
    public void testCreatePdf() throws Exception {
        System.out.println("createPdf");
        int iid = 18;
        String description = "1 ; mucahid ; benlioglu ; sabanci uni. orhanli : Asus Laptop ; 1 ; 26.0 ; 499.99 : Samsung LED TV ; 2 ; 0.0 ; 1934.19 : LG Nexus 5X ; 1 ; 21.0 ; 379.";
        final String DEST = "tests/pdfParse/text.txt";
        File file = new File(DEST);
        file.getParentFile().mkdirs();
        InvoicePdf instance = new InvoicePdf(iid, description);
        String result = instance.createPdf();
        try {
            PdfReader reader = new PdfReader(result);
            FileOutputStream fos = new FileOutputStream(DEST);
            for (int page = 1; page < reader.getNumberOfPages(); page++) {
                fos.write(PdfTextExtractor.getTextFromPage(reader, page).getBytes("UTF-8"));
            }
            fos.flush();
            fos.close();
        } catch (IOException e) {
            System.out.println("umbrella_suite.controller.InvoicePdfTest.testCreatePdf(): Failed... Invalid pdf");
            fail(e.getMessage());
        }finally{
            File f = new File("tests/pdfParse/invoice_18_mucahid_benlioglu.pdf");
            if (file.exists()) {
                file.delete();
            }
            if (f.exists()) {
                f.delete();
            }
        }
        System.out.println("umbrella_suite.controller.InvoicePdfTest.testCreatePdf(): Succeed.");
    }
    
}
