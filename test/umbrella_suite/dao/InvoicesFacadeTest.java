/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.dao;

import java.util.List;
import javax.ejb.embeddable.EJBContainer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import umbrella_suite.model.Invoices;

/**
 *
 * @author mbenlioglu
 */
public class InvoicesFacadeTest {
    
    public InvoicesFacadeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of processInvoice method, of class InvoicesFacade.
     */
    @Test
    public void testProcessInvoice() throws Exception {
        System.out.println("umbrella_suite.dao.InvoicesFacadeTest.testProcessInvoice(): test started");
        String address = "test address";
        String description = "test description";
        int oid = 0;
        InvoicesFacadeLocal instance = new InvoicesFacade();
        Invoices expResult = new Invoices();
        expResult.setAddress(address);
        expResult.setDescription(description);
        expResult.setOid(oid);
        Invoices result = instance.processInvoice(address, oid, description);
        try {
            assertEquals(expResult.getAddress(), result.getAddress());
            System.out.println("umbrella_suite.dao.InvoicesFacadeTest.testProcessInvoice(): Adress OK.");
            assertEquals(expResult.getDescription(), result.getDescription());
            System.out.println("umbrella_suite.dao.InvoicesFacadeTest.testProcessInvoice(): Description OK");
            assertEquals(expResult.getOid(), result.getOid());
            System.out.println("umbrella_suite.dao.InvoicesFacadeTest.testProcessInvoice(): Oid OK");
        } catch (Exception e) {
            System.out.println("umbrella_suite.dao.InvoicesFacadeTest.testProcessInvoice(): Wrong process output");
            fail(e.getMessage());
        }
        System.out.println("umbrella_suite.dao.InvoicesFacadeTest.testProcessInvoice(): Test Succeed");
    }
    
}
