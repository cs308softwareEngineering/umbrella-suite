/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.dao;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.embeddable.EJBContainer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author Mert
 */
public class CryptoFacadeTest {

    public CryptoFacadeTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of hashPassword method, of class CryptoFacade.
     */
    @Test
    public void testHashPassword() throws Exception {
        System.out.println("testhashPassword Started");
        String password = "12345678";
        System.out.println("Password = " + password);
        String salt = "1234";
        System.out.println("Salt = " + salt);
        String expResult = "fc2f04f00c23a34e94abdf5a6ad2f768c7310d032d66cb2245180c5761e69ec4";
        System.out.println("Excepted hash = " + expResult);
        int expLength = 64;
        System.out.println("Excepted length of hash = " + expLength);

        CryptoFacadeLocal instance = new CryptoFacade();
        String hash = instance.hashPassword(password, salt);
        int correctLen = hash.length();
        System.out.println("Correct hash = " + hash);
        System.out.println("Correct hash length = " + correctLen);

        try {
            assertEquals(expResult, hash);
            assertEquals(expLength, correctLen);
        } catch (AssertionError ae) {
            System.out.println(ae.getMessage());
            fail("The test case is a prototype.");
        }
        System.out.println("umbrella_suite.dao.CryptoFacadeTest.testHashPassword() success!");
    }

    /**
     * Test of digestText method, of class CryptoFacade.
     */
    @Test
    public void testDigestText() throws Exception {
        System.out.println("testdigestText Started");
        String text = "cs308";
        System.out.println("Text = " + text);
        String algorithm = "SHA-256";
        System.out.println("Algorithm = " + algorithm);
        String Expdigest = "51164c96536ecd6e4dd3436450ce50a7fd93f4fec2a87518881524ce5094199a";
        Integer lenExp = Expdigest.length();
        System.out.println("Expected Digest = " + Expdigest);
        System.out.println("Length of ExpectedDigest " + lenExp);

        CryptoFacadeLocal instance = new CryptoFacade();
        String digest = instance.digestText(text, algorithm);
        Integer lenDigest = digest.length();
        System.out.println("DigestResult = " + digest);
        System.out.println("DigestResultLength = " + lenDigest);
        try {
            assertEquals(Expdigest, digest);
            assertEquals(lenExp, lenDigest);
        } catch (AssertionError ae) {
            System.out.println(ae.getMessage());
            fail("The test case is a prototype.");
        }
        System.out.println("umbrella_suite.dao.CryptoFacadeTest.testDigestText() success!");
    }

    /**
     * Test of saltGenerator method, of class CryptoFacade.
     */
    @Test
    public void testSaltGenerator() throws Exception {
        System.out.println("saltGeneratorTest Started");
        Integer expResult = 4;

        CryptoFacadeLocal instance = new CryptoFacade();

        Integer resultLen = instance.saltGenerator().length();
        try {
            assertEquals(expResult, resultLen);
        } catch (AssertionError ae) {
            System.out.println(ae.getMessage());
            fail("The test case is a prototype.");
        }
        System.out.println("umbrella_suite.dao.CryptoFacadeTest.testSaltGenerator() success!");
    }

}
