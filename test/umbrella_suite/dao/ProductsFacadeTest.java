/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.ejb.embeddable.EJBContainer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import umbrella_suite.model.Products;

/**
 *
 * @author mbenlioglu
 */
public class ProductsFacadeTest {
    
    public ProductsFacadeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of processProduct method, of class ProductsFacade.
     */
    @Test
    public void testProcessProduct() throws Exception {
        System.out.println("umbrella_suite.dao.ProductsFacadeTest.testProcessProduct(): Test started...");
        String productName3 = "test product";
        Integer quantity3 = (int) Math.random();
        String department3 = "test department";
        Double newPrice3 = Math.random();
        String description3 = "test description";
        String image3 = "test image url";
        ProductsFacadeLocal instance = new ProductsFacade();
        Products expResult = new Products();
        expResult.setPname(productName3);
        expResult.setCount(quantity3);
        expResult.setPtname(department3);
        expResult.setPrice(newPrice3);
        expResult.setDiscount(0.0);
        expResult.setDescription(description3);
        expResult.setImage(image3);
        Products result = instance.processProduct(productName3, quantity3, department3, newPrice3, description3, image3);
        try {
            assertEquals(expResult.getPname(), result.getPname());
            System.out.println("umbrella_suite.dao.ProductsFacadeTest.testProcessProduct(): Product Name OK");
            assertEquals(expResult.getCount(), result.getCount());
            System.out.println("umbrella_suite.dao.ProductsFacadeTest.testProcessProduct(): Product Count OK");
            assertEquals(expResult.getPtname(), result.getPtname());
            System.out.println("umbrella_suite.dao.ProductsFacadeTest.testProcessProduct(): Departmant Name OK");
            assertEquals(expResult.getPrice(), result.getPrice());
            System.out.println("umbrella_suite.dao.ProductsFacadeTest.testProcessProduct(): Product Price OK");
            assertEquals(expResult.getDiscount(), result.getDiscount());
            System.out.println("umbrella_suite.dao.ProductsFacadeTest.testProcessProduct(): Product Disount OK");
            assertEquals(expResult.getDescription(), result.getDescription());
            System.out.println("umbrella_suite.dao.ProductsFacadeTest.testProcessProduct(): Product Description OK");
            assertEquals(expResult.getImage(), result.getImage());
            System.out.println("umbrella_suite.dao.ProductsFacadeTest.testProcessProduct(): Product Image OK");
        } catch (Exception e) {
            System.err.println("umbrella_suite.dao.ProductsFacadeTest.testProcessProduct(): Outputs mismatch, test failed!");
            fail(e.getMessage());
        }
        System.out.println("umbrella_suite.dao.ProductsFacadeTest.testProcessProduct(): Test Succeed.");
    }
    
}
