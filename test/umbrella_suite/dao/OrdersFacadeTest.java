/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.dao;

import java.util.List;
import javax.ejb.embeddable.EJBContainer;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import umbrella_suite.model.Orders;

/**
 *
 * @author mbenlioglu
 */
public class OrdersFacadeTest {
    
    public OrdersFacadeTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of processOrder method, of class OrdersFacade.
     */
    @Test
    public void testProcessOrder() throws Exception {
        System.out.println("umbrella_suite.dao.OrdersFacadeTest.testProcessOrder(): Test Started...");
        int scid = (int) Math.random();
        String expOrderStatus = "hold";
        OrdersFacadeLocal instance = new OrdersFacade();
        Orders result = instance.processOrder(scid);
        try {
            assertEquals(scid, result.getScid());
            System.out.println("umbrella_suite.dao.OrdersFacadeTest.testProcessOrder(): ShoppingCart ID OK...");
            assertEquals(expOrderStatus, result.getOrderstatus());
            System.out.println("umbrella_suite.dao.OrdersFacadeTest.testProcessOrder(): OrderStatus OK...");
        } catch (Exception e) {
            System.err.println("umbrella_suite.dao.OrdersFacadeTest.testProcessOrder(): Results not match, Test Failed...");
            fail(e.getMessage());
        }
        System.out.println("umbrella_suite.dao.OrdersFacadeTest.testProcessOrder(): Test Succeed..");
    }
    
}
