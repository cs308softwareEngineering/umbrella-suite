# Umbrella Suite

E-Business web application is designed with *waterfall software development* and implemented by *JAVA* backend, *HTML* 
and *JSP*.

**Designed and Implemented By:**

* @mertkosan
* @mbenlioglu
* @beyzabozbey
* @ulmanov

## About

This project is a web application that provides online solutions for a business to put products for sale, including a
web interface for customers, which has order tracking, customer feedback, technical support; a management web interface
for technical and management personnel that can provide technical support, answer customer tickets, edit or update
warehouse status and stock prices depending on the role and privileges of the personnel.

The application is designed and implemented by using _3-Tier software architectural pattern_.

1. **Presentation Layer**

    This layer consists of view of the system, which is interacting with user. For view part, application is using HTML,
    JSP and Java.
    
2. **Business Layer**

    Controllers and operations related database is done in this layer. JAVA is used for this layer. Also, model for
    database entities are kept in this layer to access data access layer easily. For that issue, 
    [JPA](http://www.oracle.com/technetwork/java/javaee/tech/persistence-jsp-140049.html) framework has been used.
    
3. **Data Access Layer**

    This layer keeps all entities for the application with using [MySQL](https://www.mysql.com/) database. You can also 
    find sql of the database [here](/db/umbrella_suite.sql).
    
Also, application uses many services; email-service, pdf-extraction service etc. Below you can find the architecture
design which explains application in detail.

![Architecture-Design](/docs/architecture-design.png)

For more information about design of the project: 

* [Software Design Specification](/docs/SoftwareDesignSpecification.pdf) 
* [Software Requirements Specification](/docs/SoftwareRequirementsSpecification.pdf)
* [Test Execution Report](/docs/TestExecutionReport.pdf)
* [Final Report](/docs/FinalReport.pdf)
