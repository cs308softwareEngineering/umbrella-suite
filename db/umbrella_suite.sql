-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 11, 2016 at 03:32 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `umbrella_suite`
--

-- --------------------------------------------------------

--
-- Table structure for table `authenticated`
--

CREATE TABLE IF NOT EXISTS `authenticated` (
  `token` varchar(64) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `pwd` varchar(64) NOT NULL,
  `salt` varchar(4) NOT NULL,
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`cid`, `email`, `pwd`, `salt`, `first_name`, `last_name`, `address`) VALUES
(25, 'customer@umbrella.com', 'ecacd6208db2b67c9fb62ffd747fdd4ca71d0d1312a759036fc46e02dc82f5f2', '1234', 'Customer', 'Umbrella', 'Sabanci L045'),
(28, 'mertkosan@sabanciuniv.edu', 'dd610cd0bc7bc1075f43372de474e245a2c2e15b087fa275190515940f7f96d0', 'pcgi', 'Mert', 'Kosan', 'Sabanci University');

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE IF NOT EXISTS `invoices` (
  `iid` int(11) NOT NULL AUTO_INCREMENT,
  `oid` int(11) NOT NULL,
  `description` varchar(300) DEFAULT NULL,
  `address` varchar(200) NOT NULL,
  PRIMARY KEY (`iid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`iid`, `oid`, `description`, `address`) VALUES
(37, 55, '25 ; Customer ; Umbrella ; Sabanci L045 : Adidas Sneaker ; 1 ; 0.0 ; 129.98 : G-Shock Watch ; 2 ; 65.0 ; 120.0 : Jessica Simpson Bag ; 1 ; 0.0 ; 88.', 'Sabanci L045'),
(39, 57, '28 ; Mert ; Kosan ; Sabanci University : G-Shock Watch ; 1 ; 70.0 ; 120.0 : The Resident Evil ; 1 ; 0.0 ; 34.03 : GoPro ; 1 ; 0.0 ; 429.', 'Sabanci University');

-- --------------------------------------------------------

--
-- Table structure for table `managers`
--

CREATE TABLE IF NOT EXISTS `managers` (
  `mid` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `pwd` varchar(64) NOT NULL,
  `salt` varchar(4) NOT NULL,
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `type_` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`mid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `managers`
--

INSERT INTO `managers` (`mid`, `email`, `pwd`, `salt`, `first_name`, `last_name`, `type_`) VALUES
(4, 'admin@umbrella.com', 'bc783aa0862b14e56b1830dbfd2c9530167c7667920b64d3e824b780a5923a1d', '1234', 'Admin', 'Admin', 'admin'),
(6, 'salesm@umbrella.com', 'db75363676f30ac6405035a31643486f6b7bf1252bf87f0ed67b7f2e9bdb18da', 'uyyx', 'Sales', 'Manager', 'salesmanager'),
(7, 'productm@umbrella.com', '89329b178a91a335be02969760d06df97f1c61f827b5ed387a16396678392467', 'oaug', 'Product', 'Manager', 'productmanager'),
(9, 'mert77@gmail.com', '64ed3adc65c49c827418eb8685e4f52dbe2ad7205356564a4e886c49a3f436ab', 'gyso', 'Mert1', 'Kosan2', 'technician');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `oid` int(11) NOT NULL AUTO_INCREMENT,
  `scid` int(11) NOT NULL,
  `orderstatus` enum('hold','delivered') DEFAULT NULL,
  PRIMARY KEY (`oid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=58 ;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`oid`, `scid`, `orderstatus`) VALUES
(55, 67, 'delivered'),
(57, 72, 'hold');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `pid` int(11) NOT NULL AUTO_INCREMENT,
  `pname` varchar(200) DEFAULT NULL,
  `count_` int(11) DEFAULT NULL,
  `ptname` varchar(50) NOT NULL,
  `price` double DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `image` varchar(200) NOT NULL,
  PRIMARY KEY (`pid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=93 ;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`pid`, `pname`, `count_`, `ptname`, `price`, `discount`, `description`, `image`) VALUES
(52, 'Bose Headphones', 0, 'Music', 150, 20, 'Bose QuietComfort 25 Acoustic Noise Cancelling Headphones - Apple devices, Black - Wired', 'http://ecx.images-amazon.com/images/I/71kgntub7mL._SY355_.jpg'),
(53, 'Asus Laptop', 49986, 'Electronics', 1000, 50, 'Asus F555LA-AB31 15.6-Inch Laptop (2.1 GHz Core i3-5010U Processor,4 GB RAM,500 GB Hard Drive, Windows 10), Black', 'http://ecx.images-amazon.com/images/I/51xDcikq4ML.jpg'),
(54, 'Electric Guitar', 48234, 'Music', 99.95, 20, 'Full Size Blue Electric Guitar with Amp, Case and Accessories Pack Beginner Starter Package', 'http://ecx.images-amazon.com/images/I/41FC7FHYleL._SY355_.jpg'),
(55, 'Call of Duty - PS4', 156731, 'Electronics', 79.99, 30, 'Call of Duty: Infinite Warfare - Legacy Edition - PlayStation 4', 'http://ecx.images-amazon.com/images/I/51PbcCDdiXL.jpg'),
(56, 'Nikon Camera', 93, 'Electronics', 341.49, 0, 'Nikon D3200 24.2 MP CMOS Digital SLR with 18-55mm f/3.5-5.6 Auto Focus-S DX VR NIKKOR Zoom Lens (Black) (OLD MODEL)', 'http://ecx.images-amazon.com/images/I/414TPneQOiL._SX300_.jpg'),
(57, 'Samsung LED TV', 85950, 'Electronics', 1934.19, 0, 'Samsung UN65JU7100 65-Inch 4K Ultra HD 3D Smart LED TV (2015 Model)', 'http://ecx.images-amazon.com/images/I/91AIGqvewgL._SX466_.jpg'),
(58, 'Epson Printer', 7586, 'Electronics', 89.99, 0, 'Epson WorkForce WF-3620 WiFi Direct All-in-One Color Inkjet Printer, Copier, Scanner', 'http://ecx.images-amazon.com/images/I/51zbOBen5DL._SX425_.jpg'),
(59, 'LG Nexus 5X', 8796398, 'Electronics', 379, 21, 'LG Nexus 5X Unlocked Smartphone - Black 16GB (U.S. Warranty)', 'http://ecx.images-amazon.com/images/I/81BVLBtGV%2BL._SX385_.jpg'),
(60, '25', 0, 'Music', 10, 20, '25 by Adele', 'http://ecx.images-amazon.com/images/I/81q0mwIoc0L._SY355_.jpg'),
(61, 'Yamaha Piano', 85690, 'Music', 599.99, 0, 'Yamaha P-115B 88-Key Graded Hammer Standard (GHS) Digital Piano (Black) Bundle with Knox Double X Stand and Knox Large Bench', 'http://ecx.images-amazon.com/images/I/71PnQ0tqCTL._SY355_.jpg'),
(62, 'Gammon Drum', 58960, 'Music', 249.95, 25, 'Gammon Percussion Full Size Complete Adult 5 Piece Drum Set with Cymbals Stands Stool and Sticks, Black', 'http://ecx.images-amazon.com/images/I/81lJ8ADBZTL._SX355_.jpg'),
(63, 'Baby Shoes', 455487, 'Dressing', 26, 20, 'Robeez Claire Mary Jane Flat', 'http://ecx.images-amazon.com/images/I/71RpX1feNTL._UX395_.jpg'),
(64, 'Adidas Sneaker', 5454645, 'Dressing', 101, 0, 'adidas Originals Men''s Superstar Foundation Casual Sneaker', 'http://ecx.images-amazon.com/images/I/41q9lNh71VL.jpg'),
(65, 'G-Shock Watch', 48435, 'Dressing', 120, 70, 'G-Shock GA110-1B Military Series Watch Black', 'http://ecx.images-amazon.com/images/I/81c2RNHiLQL._SY355_.jpg'),
(66, 'Steve Madden Shoes', 154, 'Dressing', 139, 80, 'STEVEN by Steve Madden Women''s Nadene Dress Pump', 'http://ecx.images-amazon.com/images/I/71TaQIp%2B25L._UX395_.jpg'),
(67, 'Adidas Cap', 965542, 'Dressing', 26, 0, 'Adidas Men''s Originals Snapback Flatbrim Cap', 'http://ecx.images-amazon.com/images/I/61qSs0Eh-hL._SY355_.jpg'),
(68, 'Jessica Simpson Bag', 55647, 'Dressing', 88, 0, 'Jessica Simpson Brandi Bucket Cross-Body Bag', 'http://ecx.images-amazon.com/images/I/91Fpq917rjL._UX385_.jpg'),
(72, 'The Resident Evil', 465843, 'Electronics', 34.03, 0, 'The Resident Evil Collection (Resident Evil / Resident Evil: Apocalypse / Resident Evil: Extinction / Resident Evil: Afterlife / Resident Evil: Retribution) [Blu-ray]', 'http://ecx.images-amazon.com/images/I/51Uo1%2B1KhiL._SY300_.jpg'),
(73, 'GoPro', 468467, 'Electronics', 429, 0, 'GoPro HERO4 BLACK', 'http://ecx.images-amazon.com/images/I/71X7ZijN3yL._SL1500_.jpg'),
(74, 'AT&T Phone', 9578, 'Electronics', 79.95, 15, 'AT&T CRL82312 DECT 6.0 Phone Answering System with Caller ID/Call Waiting, 3 Cordless Handsets, Black/Silver', 'http://ecx.images-amazon.com/images/I/81gvtg1ezhL._SY355_.jpg'),
(75, '1 TB Hard Drive', 684654, 'Electronics', 56.99, 0, 'Transcend Military Drop Tested 1 TB USB 3.0 M3 External Hard Drive (TS1TSJ25M3)', 'https://i.ytimg.com/vi/cvNph_6x9g0/hqdefault.jpg'),
(76, 'microSDXC Card', 464863, 'Electronics', 59.99, 67, 'SanDisk Ultra 64GB microSDXC UHS-I Card with Adapter, Grey/Red, Standard Packaging (SDSQUNC-064G-GN6MA)', 'http://ecx.images-amazon.com/images/I/41tfOAJ0lfL._SX300_.jpg'),
(77, 'KingPad Tablet PC', 84955, 'Electronics', 54.99, 0, 'KingPad K90 9'''' Quad Core Tablet PC, Android 4.4.4 KitKat, 8GB Nand Flash, Dual Camera, 1024x600 HD Resolution, Bluetooth, Mini HDMI', 'http://www.bestbuylaptops.net/wp-content/uploads/2015/06/706b6cdc2aea.jpg'),
(78, 'Logitech Keyboard & Mouse', 7589, 'Electronics', 41.99, 0, 'Logitech Wireless Wave Combo Mk550 With Keyboard and Laser Mouse', 'http://ecx.images-amazon.com/images/I/519nu-CTaCL._SY300_.jpg'),
(79, 'Guitar Picks', 947852, 'Music', 4.75, 59, 'D''Addario Assorted Pearl Celluloid Guitar Picks, 10 Pack, Medium', 'http://ecx.images-amazon.com/images/I/716okGFUlXL._SY355_.jpg'),
(80, 'Keyboard Bench', 8745, 'Music', 47.95, 0, 'On Stage KT7800 Plus Padded Keyboard Bench', 'http://ecx.images-amazon.com/images/I/61R23A-kKzL._SY355_.jpg'),
(81, 'Microphone', 61799, 'Music', 19.99, 20, 'Nady SP-4C Dynamic Neodymium Microphone - Professional vocal microphone for performance, stage, karaoke, public speaking, recording - includes 15'' XLR-to-1/4 cable', 'http://ecx.images-amazon.com/images/I/611D9WSe3ML._SY355_.jpg'),
(82, 'Karaoke System', 785149, 'Music', 69.99, 16, 'The Singing Machine SML-385W Disco Light Karaoke System', 'http://ecx.images-amazon.com/images/I/61dH4nkTDeL._SY355_.jpg'),
(83, 'Karaoke Pedestal', 56860, 'Music', 201.08, 0, 'Singing Machine iSM1030BT Bluetooth Karaoke Pedestal', 'http://i5.walmartimages.com/dfw/dce07b8c-fa17/k2-_16da3fec-7106-4945-ab83-0930d1ec9ff3.v1.jpg'),
(84, 'Mini Amplifier', 62472, 'Music', 22.99, 0, 'Danelectro Honeytone N-10 Guitar Mini Amp, Aqua', 'http://ecx.images-amazon.com/images/I/71-LKbV6iNL._SY355_.jpg'),
(85, 'Rain Jacket', 54745, 'Dressing', 48.82, 0, 'Columbia Men''s Watertight II Front-Zip Hooded Rain Jacket', 'http://ecx.images-amazon.com/images/I/81KBqla47QL._UX342_.jpg'),
(86, 'Batman Logo T-Shirt', 548155, 'Dressing', 9.99, 0, 'Batman Classic Logo Adult T-Shirt', 'http://ecx.images-amazon.com/images/I/51Tx5WdvbKL._UX385_.jpg'),
(87, 'Marvel Backpack', 35884, 'Dressing', 36.96, 0, 'Marvel Comic Retro Backpack', 'http://ecx.images-amazon.com/images/I/717ILWgvc7L._UL1500_.jpg'),
(88, 'Deadpool Socks', 847892, 'Dressing', 8.99, 0, 'Deadpool All Over Print Crew Socks', 'http://www.previewsworld.com/catalogimages/STK_IMAGES/STL000001-020000/STL012567.jpg'),
(89, 'Tommy Hilfiger Belt', 17855, 'Dressing', 23.64, 0, 'Tommy Hilfiger Men''s Leather Reversible Belt', 'http://ecx.images-amazon.com/images/I/413W1PT9ggL.jpg'),
(90, 'Ray-Ban Sunglasses', 4545, 'Dressing', 175, 0, '0', 'http://www.motorhelmets.com/media/products/ray-ban/2014-07/apparel-ray-ban-casual-sunglasses-polarized-rb3025-aviator-large-metal-matte-gold-brown.jpg'),
(92, 'Poeme Parfume', 200, 'Dressing', 300, 0, 'Poeme Parfume Paris', 'http://www.mamiyaleaf.com/assets/slider/product/product_slider_heinz_baumann.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `product_types`
--

CREATE TABLE IF NOT EXISTS `product_types` (
  `ptname` varchar(50) NOT NULL DEFAULT '',
  `typeDiscount` double DEFAULT NULL,
  PRIMARY KEY (`ptname`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_types`
--

INSERT INTO `product_types` (`ptname`, `typeDiscount`) VALUES
('Dressing', 0),
('Electronics', 0),
('Music', 0);

-- --------------------------------------------------------

--
-- Table structure for table `shoppingcart`
--

CREATE TABLE IF NOT EXISTS `shoppingcart` (
  `scid` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`scid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=74 ;

--
-- Dumping data for table `shoppingcart`
--

INSERT INTO `shoppingcart` (`scid`, `email`) VALUES
(67, 'customer@umbrella.com'),
(68, 'customer@umbrella.com'),
(72, 'mertkosan@sabanciuniv.edu'),
(73, 'mertkosan@sabanciuniv.edu');

-- --------------------------------------------------------

--
-- Table structure for table `shoppingcart_has_products`
--

CREATE TABLE IF NOT EXISTS `shoppingcart_has_products` (
  `scid` int(11) NOT NULL DEFAULT '0',
  `pid` int(11) NOT NULL DEFAULT '0',
  `number_of_product` int(11) DEFAULT NULL,
  PRIMARY KEY (`scid`,`pid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shoppingcart_has_products`
--

INSERT INTO `shoppingcart_has_products` (`scid`, `pid`, `number_of_product`) VALUES
(67, 64, 1),
(67, 65, 2),
(67, 68, 1),
(68, 53, 1),
(68, 56, 1),
(72, 65, 1),
(72, 72, 1),
(72, 73, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE IF NOT EXISTS `tickets` (
  `tid` int(11) NOT NULL AUTO_INCREMENT,
  `oid` int(11) NOT NULL,
  `problem` varchar(300) DEFAULT NULL,
  `reply` varchar(300) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`tid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tickets`
--

INSERT INTO `tickets` (`tid`, `oid`, `problem`, `reply`, `active`, `email`) VALUES
(8, 55, 'Everything okey!, i just want to thank you...', 'Ohhh.. Thx!', 0, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
