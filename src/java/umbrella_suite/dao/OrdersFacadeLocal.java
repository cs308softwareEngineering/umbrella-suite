/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.dao;

import java.util.List;
import javax.ejb.Local;
import umbrella_suite.model.Orders;

/**
 *
 * @author mbenlioglu
 */
@Local
public interface OrdersFacadeLocal {

    void create(Orders orders);

    void edit(Orders orders);

    void remove(Orders orders);

    Orders find(Object id);
    
    Orders findByScid(int scid); //return Orders corresponding scid //test it

    List<Orders> findAll();

    List<Orders> findRange(int[] range);

    int count();
    
    void setDelivered(int oid); //test it
    
    Orders processOrder(int scid); //test it
    
}
