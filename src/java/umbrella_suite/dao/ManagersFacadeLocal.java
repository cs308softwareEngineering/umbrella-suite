/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.dao;

import java.util.List;
import javax.ejb.Local;
import umbrella_suite.model.Managers;

/**
 *
 * @author mbenlioglu
 */
@Local
public interface ManagersFacadeLocal {

    void create(Managers managers);

    void edit(Managers managers);

    void remove(Managers managers);

    Managers find(Object id);
    
    Managers find(String email); //test it

    List<Managers> findAll();

    List<Managers> findRange(int[] range);

    int count();
    
}
