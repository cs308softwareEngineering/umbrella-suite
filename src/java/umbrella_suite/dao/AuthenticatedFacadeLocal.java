/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.dao;

import java.util.List;
import javax.ejb.Local;
import umbrella_suite.model.Authenticated;
import umbrella_suite.model.Customers;
import umbrella_suite.model.Managers;

/**
 *
 * @author mbenlioglu
 */
@Local
public interface AuthenticatedFacadeLocal {

    void create(Authenticated authenticated); 

    void edit(Authenticated authenticated);

    void remove(Authenticated authenticated); 

    Authenticated find(Object id);

    List<Authenticated> findAll();

    List<Authenticated> findRange(int[] range);

    int count();
     
    boolean checkCustomer(String email, String password, Customers temp); //password hasing will be made... For debuging it be passed now.
    Customers processCustomer(String email, String password, String firstName, String lastName, String address); //password hasing will be made... For debuging it be passed now.
    String addSession(Customers customer); //test it
    
    boolean checkManager(String email, String password, Managers temp); //password hasing will be made... For debuging it be passed now.
    Managers processManager(String email, String password, String firstName, String lastName, String type); //password hasing will be made... For debuging it be passed now.
    String addSession(Managers manager); //test it
    
    void removeSession(Authenticated user); //test it
    Authenticated findSession(String sessionID); //test it
    String generateSessionToken(String text); //test it
}
