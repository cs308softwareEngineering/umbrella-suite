/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.dao;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import umbrella_suite.model.Shoppingcart;

/**
 *
 * @author mbenlioglu
 */
@Stateless
public class ShoppingcartFacade extends AbstractFacade<Shoppingcart> implements ShoppingcartFacadeLocal {

    @PersistenceContext(unitName = "umbrella_suitePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ShoppingcartFacade() {
        super(Shoppingcart.class);
    }

    @Override
    public Shoppingcart processShoppingCart(String email) {
        Shoppingcart temp = new Shoppingcart();
        temp.setEmail(email);
           
        return temp;
    }

    @Override
    public int find(String email) {
        List<Shoppingcart> results = em.createQuery(
                "SELECT s FROM Shoppingcart s WHERE s.email= :email").setParameter("email", email).getResultList();
        if(results.isEmpty())
        {
            System.err.println("No shoppingcart with email: "+email);
            return -1;
        }
        else
        {
            System.err.println("shoppingcart with email: "+email);
            return results.get(results.size()-1).getScid();
        }
    }
    
    //return ordered shopping cart list with given email
    @Override
    public List<Shoppingcart> findOrdered(String email) {
        List<Shoppingcart> results = em.createQuery(
                "SELECT s FROM Shoppingcart s WHERE s.email= :email").setParameter("email", email).getResultList();
        if(results.isEmpty())
        {
            System.err.println("No shoppingcart with email: "+email);
            return results;
        }
        else
        {
            System.err.println("shoppingcarts with email: "+email);
            return results;
        }
    }
    
}
