/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.dao;

import javax.ejb.Local;

/**
 *
 * @author Mert
 */
@Local
public interface CryptoFacadeLocal {
    
    String hashPassword(String password, String salt);
    String digestText(String text, String algorithm);
    String saltGenerator();
    
}
