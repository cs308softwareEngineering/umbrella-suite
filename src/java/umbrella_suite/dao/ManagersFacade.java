/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.dao;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import umbrella_suite.model.Managers;

/**
 *
 * @author mbenlioglu
 */
@Stateless
public class ManagersFacade extends AbstractFacade<Managers> implements ManagersFacadeLocal {

    @PersistenceContext(unitName = "umbrella_suitePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ManagersFacade() {
        super(Managers.class);
    }

    @Override
    public Managers find(String email) {
        List<Object> results = em.createQuery(
                "SELECT m FROM Managers m WHERE m.email = :email").setParameter("email", email).getResultList();
        if(results.isEmpty())
        {
            System.err.println("No managers with email: "+email);
            return null;
        }
        else
        {
            System.err.println("managers with email: "+email);
            return (Managers)results.get(0);
        }
    }
    
}
