/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.dao;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import umbrella_suite.model.Tickets;

/**
 *
 * @author mbenlioglu
 */
@Stateless
public class TicketsFacade extends AbstractFacade<Tickets> implements TicketsFacadeLocal {

    @PersistenceContext(unitName = "umbrella_suitePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TicketsFacade() {
        super(Tickets.class);
    }
    
    @Override
    public List<Tickets> find(int oid) {
        List<Tickets> results = em.createQuery(
                "SELECT t FROM Tickets t WHERE t.oid = :oid").setParameter("oid", oid).getResultList();
        if(results.isEmpty())
        {
            System.err.println("No tickets with oid: "+oid);
            return results;
        }
        else
        {
            System.err.println("ticket with oid: "+oid);
            return results;
        }
    }

    @Override
    public Tickets processTicket(int oid, String problem) {

        Tickets temp = new Tickets();
        temp.setOid(oid);
        temp.setProblem(problem);
        temp.setActive(Boolean.TRUE);
        temp.setReply("");
        return temp;
    }

    @Override
    public void setResolved(Integer tid) {
        Tickets ticket = this.find(tid);
        if(ticket != null){
            ticket.setActive(Boolean.FALSE);
            this.edit(ticket);
            System.err.println("Ticket is set to resolved ! (with tid = " + tid);
        }else{
            System.err.println("no Ticket found with tid " + tid);
        }
    }

    @Override
    public void replyTicket(String reply, Integer tid) {
        Tickets ticket = this.find(tid);
        if(ticket != null){
            ticket.setReply(reply);
            this.edit(ticket);
            StringBuilder builder = new StringBuilder();
            builder.append("Ticket is replied ! (with tid = ").append(tid).append(" with reply = ").append(reply).append(")");
            System.err.println(builder.toString().trim());
        }else{
            System.err.println("no Ticket found to be replied with tid " + tid);
        }
    }
    
    

    
}
