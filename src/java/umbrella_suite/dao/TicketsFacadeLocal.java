/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.dao;

import java.util.List;
import javax.ejb.Local;
import umbrella_suite.model.Tickets;

/**
 *
 * @author mbenlioglu
 */
@Local
public interface TicketsFacadeLocal {

    void create(Tickets tickets);

    void edit(Tickets tickets);

    void remove(Tickets tickets);

    Tickets find(Object id);
    
    List<Tickets> find(int oid); //given Order, find its ticket //test it

    List<Tickets> findAll();

    List<Tickets> findRange(int[] range);

    int count();
    
    Tickets processTicket(int oid, String problem); // test it
    
    void setResolved(Integer tid);
    
    void replyTicket(String reply, Integer tid);
    
}
