/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.dao;

import java.util.Calendar;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import umbrella_suite.model.Authenticated;
import umbrella_suite.model.Customers;
import umbrella_suite.model.Managers;
import javax.inject.Inject;

/**
 *
 * @author mbenlioglu
 */
@Stateless
public class AuthenticatedFacade extends AbstractFacade<Authenticated> implements AuthenticatedFacadeLocal {

    @Inject
    CryptoFacadeLocal crypto;

    @PersistenceContext(unitName = "umbrella_suitePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AuthenticatedFacade() {
        super(Authenticated.class);
    }

    @Override
    //checks Customer credentials
    public boolean checkCustomer(String email, String password, Customers temp) {
        String salt = temp.getSalt();//crypto.saltGenerator().trim();
        String hash = crypto.hashPassword(password, salt);
        if (hash.equals(temp.getPwd())) {
            System.err.println("customer email: " + temp.getEmail());
            return true;
        } else {
            return false;
        }
    }

    //create and return Customer object
    @Override
    public Customers processCustomer(String email, String password, String firstName, String lastName, String address) {
        String salt = crypto.saltGenerator().trim();
        String hash = crypto.hashPassword(password, salt);
        if (hash != null) {
            Customers temp = new Customers();
            temp.setEmail(email);
            temp.setPwd(hash);
            temp.setPwd(hash); 
            temp.setSalt(salt);
            temp.setFirstName(firstName);
            temp.setLastName(lastName);
            temp.setAddress(address);

            return temp;
        }
        return null;
    }

    //check Manager credentials
    @Override
    public boolean checkManager(String email, String password, Managers temp) {
        String salt = temp.getSalt();//crypto.saltGenerator().trim();
        String hash = crypto.hashPassword(password, salt);
        if (hash.equals(temp.getPwd())) {
            System.err.println("manager email: " + temp.getEmail());
            return true;
        } else {
            return false;
        }
    }

    //below codes related to session (token), when user sign-in session token will be created, when sign-up, token will be destoyed from the system.
    @Override
    public String addSession(Customers customer) {
        Authenticated session
                = //creates session by using token and  email
                new Authenticated(
                        this.generateSessionToken( // generating token by using email
                                customer.getEmail().concat(
                                        Calendar.getInstance().getTime().toString()
                                )
                        ), customer.getEmail());
        this.create(session);

        return session.getToken();
    }

    @Override
    public String addSession(Managers manager) {
        Authenticated session
                = //creates session by using token and  email
                new Authenticated(
                        this.generateSessionToken( // generating token by using email
                                manager.getEmail().concat(
                                        Calendar.getInstance().getTime().toString()
                                )
                        ), manager.getEmail());
        this.create(session);

        return session.getToken();
    }

    @Override
    public void removeSession(Authenticated user) {
        String token = user.getToken();
        System.err.println(token);
        Authenticated auth = this.find(token);
        this.remove(auth);
    }

    @Override
    public Authenticated findSession(String token) {
        return this.find(token);
    }

    @Override
    public String generateSessionToken(String text) {
        return crypto.digestText(text, "SHA-256");
    }

    @Override
    public Managers processManager(String email, String password, String firstName, String lastName, String type) {
        String salt = crypto.saltGenerator().trim();
        String hash = crypto.hashPassword(password, salt);
        if (hash != null) {
            Managers temp = new Managers();
            temp.setEmail(email);
            temp.setPwd(hash);
            temp.setSalt(salt);
            temp.setFirstName(firstName);
            temp.setLastName(lastName);
            temp.setType(type);

            return temp;
        }
        return null;
    }

}
