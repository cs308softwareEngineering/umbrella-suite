/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.dao;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import umbrella_suite.model.ShoppingcartHasProducts;
import umbrella_suite.model.ShoppingcartHasProductsPK;

/**
 *
 * @author mbenlioglu
 */
@Stateless
public class ShoppingcartHasProductsFacade extends AbstractFacade<ShoppingcartHasProducts> implements ShoppingcartHasProductsFacadeLocal {

    @PersistenceContext(unitName = "umbrella_suitePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ShoppingcartHasProductsFacade() {
        super(ShoppingcartHasProducts.class);
    }
    
    @Override
    public ArrayList<ShoppingcartHasProducts> find(int scid) {
        List<ShoppingcartHasProducts> results = em.createQuery(
                "SELECT s FROM ShoppingcartHasProducts s WHERE s.shoppingcartHasProductsPK.scid = :scid").setParameter("scid", scid).getResultList();
        if(results.isEmpty())
        {
            System.err.println("No shoppingcarthasproducts with scid: "+scid);
            return null;
        }
        else
        {
            System.err.println("shoppingcarthasproducts with scid: "+scid);
            ArrayList<ShoppingcartHasProducts> results2 = new ArrayList<>();
            for(int i=0; i<results.size(); i++){
               results2.add(results.get(i));
            }
            return  results2;
        }
    }

    @Override
    public ShoppingcartHasProducts processProductShopping(int scid, int pid) {
            
            
            ShoppingcartHasProducts temp = new ShoppingcartHasProducts();
            ShoppingcartHasProductsPK hasProductsPK = new ShoppingcartHasProductsPK(scid, pid);
            temp.setShoppingcartHasProductsPK(hasProductsPK);
            temp.setNumberOfProduct(1);
            return temp;
    }

    @Override
    public ShoppingcartHasProducts find(int scid, int pid) { //find row that customer's shopping cart and its corresponding product
        List<ShoppingcartHasProducts> results = em.createQuery(
                "SELECT s FROM ShoppingcartHasProducts s WHERE s.shoppingcartHasProductsPK.scid = :scid AND s.shoppingcartHasProductsPK.pid = :pid")
                .setParameter("scid", scid).setParameter("pid", pid).getResultList();
        if(results.isEmpty())
        {
            System.err.println("No shoppingcarthasproducts with scid and pid: "+scid + " and " + pid);
            return null;
        }
        else
        {
            System.err.println("shoppingcarthasproducts with scid and pid: "+scid + " and " + pid);
            return results.get(0);
        }
    }

    @Override
    public void incrementProductQuantity(ShoppingcartHasProducts hasProducts) {
        hasProducts.setNumberOfProduct(hasProducts.getNumberOfProduct()+1);
        this.edit(hasProducts);
    }

    @Override
    public int countByScid(int scid) {
        List<ShoppingcartHasProducts> results = em.createQuery(
                "SELECT s FROM ShoppingcartHasProducts s WHERE s.shoppingcartHasProductsPK.scid = :scid").setParameter("scid", scid).getResultList();
        if(results.isEmpty())
        {
            System.err.println("No shoppingcarthasproducts with scid: "+scid);
            return 0;
        }
        else
        {
            System.err.println("shoppingcarthasproducts with scid: "+scid);
            return results.size();
        }
    }

    
    
}
