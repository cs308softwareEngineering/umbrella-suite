/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author mbenlioglu
 */
@Embeddable
public class ShoppingcartHasProductsPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "scid")
    private int scid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "pid")
    private int pid;

    public ShoppingcartHasProductsPK() {
    }

    public ShoppingcartHasProductsPK(int scid, int pid) {
        this.scid = scid;
        this.pid = pid;
    }

    public int getScid() {
        return scid;
    }

    public void setScid(int scid) {
        this.scid = scid;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) scid;
        hash += (int) pid;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ShoppingcartHasProductsPK)) {
            return false;
        }
        ShoppingcartHasProductsPK other = (ShoppingcartHasProductsPK) object;
        if (this.scid != other.scid) {
            return false;
        }
        if (this.pid != other.pid) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "umbrella_suite.model.ShoppingcartHasProductsPK[ scid=" + scid + ", pid=" + pid + " ]";
    }
    
}
