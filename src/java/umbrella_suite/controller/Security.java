package umbrella_suite.controller;

import java.math.BigInteger;
import java.security.SecureRandom;

public class Security {

	private static SecureRandom random = new SecureRandom();
	
	public static String generatePassword(){ // test it...
		
		return new BigInteger(130, random).toString(32).substring(0, 8);
	}
	
}
