/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package umbrella_suite.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import umbrella_suite.dao.*;
import umbrella_suite.model.*;

/**
 *
 * @author Mert
 */
@WebServlet(name = "ManagerController", urlPatterns = {"/ManagerController"})
public class ManagerController extends HttpServlet {

    @EJB
    private ProductsFacadeLocal productBean;
    @EJB
    private TicketsFacadeLocal ticketBean;
    @EJB
    private CustomersFacadeLocal customerBean;
    @EJB
    private ManagersFacadeLocal managerBean;
    @EJB
    private ShoppingcartFacadeLocal shoppingCartBean;
    @EJB
    private OrdersFacadeLocal orderBean;
    @EJB
    private InvoicesFacadeLocal invoiceBean;
    @EJB
    private AuthenticatedFacadeLocal authBean;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getParameter("action");

        switch (action) {

            //Sales Manager get all product function
            case "productPageSales":

                List<Products> products = productBean.findAll();
                request.setAttribute("products", products);
                request.getRequestDispatcher("Jsp/ProductManagementPage.jsp").forward(request, response);
                break;

            //Sales Manager set price function
            case "setPrice":

                Integer pid = Integer.parseInt(request.getParameter("productIdField"));
                String priceStr = request.getParameter("productPriceField");
                String discountStr = request.getParameter("productDiscountField");
                boolean check2 = true;

                Double newPrice = null,
                 newDiscount = null;
                try {
                    newPrice = Double.parseDouble(priceStr);
                    newDiscount = Double.parseDouble(discountStr);
                } catch (NumberFormatException e) {
                    String message = "Please enter proper input for price and discount";
                    request.setAttribute("exception", message);
                    check2 = false;
                    request.getRequestDispatcher("Jsp/Exception.jsp").forward(request, response);
                }

                if (check2) {
                    productBean.setNewPrices(pid, newPrice, newDiscount);

                    List<Products> products3 = productBean.findAll();
                    request.setAttribute("products", products3);
                    request.getRequestDispatcher("Jsp/ProductManagementPage.jsp").forward(request, response);
                }

                break;

            //Product Manager get all product function
            case "stockManagementPage":

                List<Products> products2 = productBean.findAll();
                request.setAttribute("products", products2);
                request.getRequestDispatcher("Jsp/StockPage.jsp").forward(request, response);
                break;

            //Product manager, get quantity set new quantity of product...
            case "setQuantity":

                Integer pid2 = Integer.parseInt(request.getParameter("productIdField"));
                String quantityStr = request.getParameter("productQuantityField");
                Integer quantity2 = 0;
                boolean check = true;

                try {
                    quantity2 = Integer.parseInt(quantityStr);
                } catch (NumberFormatException e) {
                    String message = "Please enter proper input for quantity";
                    request.setAttribute("exception", message);
                    check = false;
                    request.getRequestDispatcher("Jsp/Exception.jsp").forward(request, response);
                }

                if (check == true) {
                    productBean.setQuantity(pid2, quantity2);

                    List<Products> products4 = productBean.findAll();
                    request.setAttribute("products", products4);
                    request.getRequestDispatcher("Jsp/StockPage.jsp").forward(request, response);
                }

                break;

            //Product Manager add new Product to database     
            case "addProduct":

                String productName3 = request.getParameter("productName");
                String department3 = request.getParameter("productType");
                String description3 = request.getParameter("description");
                String image3 = request.getParameter("imageUrl");
                String quantity3Str = request.getParameter("quantity");
                String newPrice3Str = request.getParameter("price");
                boolean check1 = true;

                if (productName3 != null && !productName3.isEmpty()
                        && department3 != null && !department3.isEmpty()
                        && description3 != null && !description3.isEmpty()
                        && image3 != null && !image3.isEmpty()
                        && quantity3Str != null && !quantity3Str.isEmpty()
                        && newPrice3Str != null && !newPrice3Str.isEmpty()) {

                    Integer quantity3 = null;
                    Double newPrice3 = null;
                    try {
                        quantity3 = Integer.parseInt(quantity3Str);
                        newPrice3 = Double.parseDouble(newPrice3Str);
                    } catch (NumberFormatException e) {
                        String message = "Please enter proper input for price and quantity";
                        request.setAttribute("exception", message);
                        check1 = false;
                        request.getRequestDispatcher("Jsp/Exception.jsp").forward(request, response);
                    }

                    if (check1) {
                        Products products5 = productBean.processProduct(productName3, quantity3, department3, newPrice3, description3, image3);
                        if (products5 != null) {
                            productBean.create(products5);
                        } else {
                            System.err.println("Product cannot be created by Product Manager... EXCEPTION!!!");
                        }

                        List<Products> products6 = productBean.findAll();
                        request.setAttribute("products", products6);
                        request.getRequestDispatcher("Jsp/StockPage.jsp").forward(request, response);
                    }

                    break;

                } else { //some fields are missing...
                    String message = "Some fields are missing!";
                    request.setAttribute("exception", message);
                    request.getRequestDispatcher("Jsp/Exception.jsp").forward(request, response);
                }

            //Product Manager looks invoices
            case "lookInvoices":
                List<Invoices> invoices = invoiceBean.findAll();
                ArrayList<Customers> customers2 = new ArrayList<>();
                ArrayList<Shoppingcart> shoppingcarts2 = new ArrayList<>();
                ArrayList<Orders> orders2 = new ArrayList<>();

                for (int i = 0; i < invoices.size(); i++) {
                    Orders orders1 = orderBean.find(invoices.get(i).getOid()); // Orders find by oid
                    if (orders1 != null) {
                        orders2.add(orders1);
                    } else {
                        System.err.println("order not found! in case ManagerController:lookInvoices...");
                    }
                }

                for (int i = 0; i < orders2.size(); i++) {
                    Shoppingcart shoppingcart = shoppingCartBean.find(orders2.get(i).getScid()); // Shopping cart find scid                                                           
                    if (shoppingcart != null) {
                        shoppingcarts2.add(shoppingcart);
                    } else {
                        System.err.println("shoppingcart not found! in case ManagerController:lookInvoices...");
                    }
                }

                for (int i = 0; i < shoppingcarts2.size(); i++) {
                    Customers customers1 = customerBean.find(shoppingcarts2.get(i).getEmail()); // Customer find by email                                                          
                    if (customers1 != null) {
                        customers2.add(customers1);
                    } else {
                        System.err.println("customers not found! in case ManagerController:lookInvoices...");
                    }
                }

                request.setAttribute("invoices", invoices);
                request.setAttribute("customers2", customers2);
                request.getRequestDispatcher("Jsp/InvoicesPage.jsp").forward(request, response);
                break;

            // Technician set the Ticket Active or reply the Ticket, change reply etc...
            case "replyTicket":

                String reply = request.getParameter("reply");
                Integer tid = Integer.parseInt(request.getParameter("tid"));

                if (reply != null || !reply.isEmpty()) {
                    ticketBean.replyTicket(reply, tid);
                }

            //not break because after every changes below case will be run...
            case "changeStatus":

                String tidStr = request.getParameter("ticketId");

                if (tidStr != null) {
                    Integer tid2 = Integer.parseInt(tidStr);
                    ticketBean.setResolved(tid2);
                }

            //Technician ticket page
            case "ticketsPage":

                List<Tickets> tickets = ticketBean.findAll();
                ArrayList<Customers> customers = new ArrayList<>();
                ArrayList<Shoppingcart> shoppingcarts = new ArrayList<>();
                ArrayList<Orders> orders = new ArrayList<>();

                for (int i = 0; i < tickets.size(); i++) {
                    Orders orders1 = orderBean.find(tickets.get(i).getOid()); // Orders find by oid
                    if (orders1 != null) {
                        orders.add(orders1);
                    }
                }
                for (int i = 0; i < orders.size(); i++) {
                    Shoppingcart shoppingcart = shoppingCartBean.find(orders.get(i).getScid()); // Shopping cart find scid
                    if (shoppingcart != null) {
                        shoppingcarts.add(shoppingcart);
                    }
                }
                for (int i = 0; i < shoppingcarts.size(); i++) {
                    Customers customers1 = customerBean.find(shoppingcarts.get(i).getEmail()); // Customer find by email
                    if (customers1 != null) {
                        customers.add(customers1);
                    }
                }
                //paralel tickets and customers array...
                request.setAttribute("tickets", tickets);
                request.setAttribute("customers1", customers);

                request.getRequestDispatcher("/Jsp/TicketPage.jsp").forward(request, response);
                break;

            //admin add manager
            case "addManager":

                String newEmail = request.getParameter("managerEmail");
                String firstName = request.getParameter("managerFirstName");
                String lastName = request.getParameter("managerLastName");
                String type = request.getParameter("managerRole");

                if (newEmail != null && !newEmail.isEmpty()
                        && firstName != null && !firstName.isEmpty()
                        && lastName != null && !lastName.isEmpty()
                        && type != null && !type.isEmpty()) {

                    Customers c = customerBean.find(newEmail);
                    Managers m = managerBean.find(newEmail);

                    if (m == null && c == null) {

                        String passwordGenerated = Security.generatePassword(); //auto generation of password

                        Managers newManager = authBean.processManager(newEmail, passwordGenerated, firstName, lastName, type);
                        if (newManager != null) {
                            managerBean.create(newManager);
                        }

                        //send Email to manager
                        String from = "umbrella.suite@gmail.com";
                        String message = "Welcome " + firstName + "!\n\nHere's your manager password: "
                                + passwordGenerated + "\nNow, you can sign in to your account and edit the website as a " + type + ".\n"
                                + "Good work and thanks for choosing us!\n\nUmbrella Suite Team.";
                        String subject = "Welcome to Umbrella Suite as a Manager!";
                        String smtp = "smtp.gmail.com";
                        Mail mail = new Mail();
                        mail.setFrom(from);
                        mail.setTo(newEmail);
                        mail.setSubject(subject);
                        mail.setMessage(message);
                        mail.setSmtpServ(smtp);
                        int successful = mail.sendMail(0);
                        if (successful == 0) {
                            System.err.println("Generated password is NOT sent to " + newEmail);
                        }

                    } else {
                        String message = "Already defined email!";
                        request.setAttribute("exception", message);
                        request.getRequestDispatcher("Jsp/Exception.jsp").forward(request, response);
                    }

                } else { // some field isn't filled by the admin
                    String message = "Some fields are missing!";
                    request.setAttribute("exception", message);
                    request.getRequestDispatcher("Jsp/Exception.jsp").forward(request, response);
                }

            //admin removes manager    
            case "removeManager":

                String midStr = request.getParameter("managerId");

                if (midStr != null) {
                    Integer mid = Integer.parseInt(midStr);
                    Managers manager = managerBean.find(mid);
                    managerBean.remove(manager);
                }

            //admin page
            case "managerPage":

                List<Managers> managers = managerBean.findAll();

                //removes the admin from list
                for (Iterator<Managers> iter = managers.listIterator(); iter.hasNext();) {
                    Managers m = iter.next();
                    if (m.getType().equals("admin")) {
                        iter.remove();
                    }
                }
                request.setAttribute("managers", managers);
                request.getRequestDispatcher("/Jsp/ManagerPage.jsp").forward(request, response);

                break;

            default:
                break;
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }
}
