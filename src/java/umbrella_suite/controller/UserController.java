package umbrella_suite.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.List;

import umbrella_suite.dao.*;
import umbrella_suite.model.*;

@WebServlet(name = "UserController", urlPatterns = {"/UserController"})
public class UserController extends HttpServlet {

    @EJB
    private CustomersFacadeLocal customerBean;
    @EJB
    private ManagersFacadeLocal managerBean;
    @EJB
    private AuthenticatedFacadeLocal authBean;
    @EJB
    private ProductTypesFacadeLocal productTypesBean;
    @EJB
    private ProductsFacadeLocal productBean;
    @EJB
    private ShoppingcartHasProductsFacadeLocal shoppingCartProductBean;
    @EJB
    private ShoppingcartFacadeLocal shoppingCartBean;
    @EJB
    private TicketsFacadeLocal ticketBean;
    @EJB
    private OrdersFacadeLocal orderBean;
    @EJB
    private InvoicesFacadeLocal invoiceBean;
    @EJB
    private CryptoFacadeLocal cryptoBean;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getParameter("action");

        switch (action) {

            //SignIn case
            case "login":
                String email = request.getParameter("email");
                String password = request.getParameter("password");

                //firstly, Manager table will be checked.
                Managers tempManager = managerBean.find(email);
                if (tempManager == null) {
                    //if this is not Manager, check if it is Customer
                    Customers tempCustomer = customerBean.find(email);
                    if (tempCustomer == null) { //not user available with this emails   
                        String message = "Wrong email address";
                        request.setAttribute("exception", message);
                        request.getRequestDispatcher("Jsp/Exception.jsp").forward(request, response);
                    } else //if(tempCustomer != null)
                    {
                        if (authBean.checkCustomer(email, password, tempCustomer)) { //checking credentials

                            //create new session, and give token to the session
                            String sessionToken = authBean.addSession(tempCustomer);
                            request.getSession(true).setAttribute("authToken", sessionToken);
                            request.getSession(true).setAttribute("email", email);
                            request.getSession(true).setAttribute("firstName", tempCustomer.getFirstName());
                            request.getSession(true).setAttribute("lastName", tempCustomer.getLastName());
                            request.getSession(true).setAttribute("address", tempCustomer.getAddress());
                            request.getSession(true).setAttribute("userType", "customer");

                            int scid = shoppingCartBean.find(email);// getting shopping cart of customer

                            int numberOfProductOnSC = shoppingCartProductBean.countByScid(scid);
                            request.getSession(true).setAttribute("cartCount", numberOfProductOnSC);
                            request.getRequestDispatcher("/Jsp/index.jsp").forward(request, response);

                        } else { //wrong password, if(!authBean.checkCustomer(email, password, tempCustomer))	
                            String message = "Wrong password!";
                            request.setAttribute("exception", message);
                            request.getRequestDispatcher("Jsp/Exception.jsp").forward(request, response);
                        }
                    }
                } else//if(tempManager != null)
                {
                    if (authBean.checkManager(email, password, tempManager)) { //checking credentials
                        //checking which manager is she?
                        String typeOfManager = tempManager.getType();

                        //create new session, and give token to the session
                        String sessionToken = authBean.addSession(tempManager);
                        request.getSession(true).setAttribute("authToken", sessionToken);

                        request.getSession(true).setAttribute("email", email);
                        request.getSession(true).setAttribute("firstName", tempManager.getFirstName());
                        request.getSession(true).setAttribute("lastName", tempManager.getLastName());
                        request.getSession(true).setAttribute("userType", typeOfManager);

                        request.getRequestDispatcher("/Jsp/index.jsp").forward(request, response);
                    } else { //wrong password, if(!authBean.checkManager(email, password, tempManager))
                        String message = "Wrong Manager password!";
                        request.setAttribute("exception", message);
                        request.getRequestDispatcher("Jsp/Exception.jsp").forward(request, response);
                    }
                }
                break;

            //SignUp case
            case "signUp": //if user click signup button

                String newEmail = request.getParameter("email");
                String firstName = request.getParameter("firstName");
                String lastName = request.getParameter("lastName");
                String address = request.getParameter("address");
                //...

                if (newEmail != null && !newEmail.trim().isEmpty()
                        && firstName != null && !firstName.trim().isEmpty()
                        && lastName != null && !lastName.trim().isEmpty()
                        && address != null && !address.trim().isEmpty()) {

                    Customers c = customerBean.find(newEmail);

                    if (c == null) {
                        String passwordGenerated = Security.generatePassword(); //auto generation of password

                        Customers newCustomer = authBean.processCustomer(newEmail, passwordGenerated, firstName, lastName, address);
                        if (newCustomer != null) {
                            customerBean.create(newCustomer);
                        }

                        //send Email to customer
                        String from = "umbrella.suite@gmail.com";
                        String message = "Welcome " + firstName + "!\n\nHere's your customer password: "
                                + passwordGenerated + "\nNow, you can sign in to your account and edit your profile.\n"
                                + "Happy shopping and thanks for choosing us!\n\nUmbrella Suite Team.";
                        String subject = "Welcome to Umbrella Suite!";
                        String smtp = "smtp.gmail.com";
                        Mail mail = new Mail();
                        mail.setFrom(from);
                        mail.setTo(newEmail);
                        mail.setSubject(subject);
                        mail.setMessage(message);
                        mail.setSmtpServ(smtp);
                        int successful = mail.sendMail(0);
                        if (successful == 0) {
                            System.err.println("Generated password is NOT sent to " + newEmail);
                        }

                        //shoppingCartUpdate, addNewShoppingCart to the user.
                        Shoppingcart shoppingcart = shoppingCartBean.processShoppingCart(newEmail);
                        if (shoppingcart != null) {
                            shoppingCartBean.create(shoppingcart);
                        }

                        //successfully register!
                        request.getRequestDispatcher("/Jsp/index.jsp").forward(request, response);
                    } else {
                        String message = "This email already defined!";
                        request.setAttribute("exception", message);
                        request.getRequestDispatcher("Jsp/Exception.jsp").forward(request, response);
                    }
                } else { //some field isn't filled by the user. 
                    String message = "Some fields are missing!";
                    request.setAttribute("exception", message);
                    request.getRequestDispatcher("Jsp/Exception.jsp").forward(request, response);
                }

                break;

            case "signOut":

                String sessionToken = (String) request.getSession().getAttribute("authToken");
                authBean.removeSession(authBean.findSession(sessionToken));
                request.getSession().removeAttribute("authToken");
                request.getSession().invalidate();
                request.getRequestDispatcher("/Jsp/index.jsp").forward(request, response);
                break;

            case "changeAddress":

                Authenticated authToken2 = authBean.findSession(
                        (String) request.getSession().getAttribute("authToken"));
                String emailCustomer2 = authToken2.getEmail();

                String address2 = request.getParameter("address");

                if (address2 != null && !address2.trim().isEmpty()) {
                    customerBean.setNewAddress(emailCustomer2, address2);
                    request.getSession(true).setAttribute("address", address2);

                    request.getRequestDispatcher("/Jsp/index.jsp").forward(request, response);
                } else { //address null or empty
                    String message = "Address field is missing!";
                    request.setAttribute("exception", message);
                    request.getRequestDispatcher("/Jsp/Exception.jsp").forward(request, response);
                }

                break;

            case "changePassword":

                Authenticated authToken3 = authBean.findSession(
                        (String) request.getSession().getAttribute("authToken"));
                String emailCustomer3 = authToken3.getEmail();

                String password3 = request.getParameter("newPassword");
                String oldPassword = request.getParameter("oldPassword");

                System.err.println(password3);
                System.err.println(oldPassword);

                if (password3 != null && !password3.trim().isEmpty()) {

                    Customers c = customerBean.find(emailCustomer3);
                    if (authBean.checkCustomer(emailCustomer3, oldPassword, c)) {
                        String hash = cryptoBean.hashPassword(password3, c.getSalt());
                        c.setPwd(hash);
                        customerBean.edit(c);
                        request.getRequestDispatcher("/Jsp/index.jsp").forward(request, response);
                    } else {
                        String message = "You have entered wrong password!";
                        request.setAttribute("exception", message);
                        request.getRequestDispatcher("/Jsp/Exception.jsp").forward(request, response);
                    }

                } else { //new_password null or empty
                    String message = "Password field is missing!";
                    request.setAttribute("exception", message);
                    request.getRequestDispatcher("/Jsp/Exception.jsp").forward(request, response);
                }

                break;

            //add ticket on specific Order
            case "addTickets":

                Integer oid = Integer.parseInt(request.getParameter("orderIdField"));
                String problem = request.getParameter("problem");
                boolean check = true;

                if (problem == null || problem.trim().isEmpty()) {
                    String message = "Problem is written wrongly!";
                    request.setAttribute("exception", message);
                    check = false;
                    request.getRequestDispatcher("/Jsp/Exception.jsp").forward(request, response);
                }

                if (check) {
                    Tickets newTicket = ticketBean.processTicket(oid, problem);
                    if (newTicket != null) {
                        ticketBean.create(newTicket);
                    } else {
                        System.err.println("Ticket is not created on oid : " + oid);
                    }
                }

            //Event continoius to setDeliveredforOrder
            //set Order as delivered...
            case "setDelieveredforOrder":

                int oid2;
                String oidStr = request.getParameter("orderId");
                if (oidStr != null) {
                    oid2 = Integer.parseInt(oidStr);
                    orderBean.setDelivered(oid2);
                }

            //Event continious to lookOrders Case 
            //User look order and give their prices...
            case "lookOrders":

                Authenticated authToken = authBean.findSession(
                        (String) request.getSession().getAttribute("authToken"));
                String emailCustomer = authToken.getEmail();

                List<Shoppingcart> shoppingcarts = shoppingCartBean.findOrdered(emailCustomer);
                ArrayList<Orders> orders = new ArrayList<>();
                ArrayList<ShoppingcartHasProducts> hasProducts = new ArrayList<>();
                ArrayList<Double> prices = new ArrayList<>();
                //get total price...

                for (int i = 0; i < shoppingcarts.size() - 1; i++) {
                    Double totalPrice = 0.0;
                    int scid = shoppingcarts.get(i).getScid();
                    hasProducts = shoppingCartProductBean.find(scid);
                    Orders order = orderBean.findByScid(scid);
                    orders.add(order);
                    int oid1 = order.getOid();

                    Invoices invoice = invoiceBean.findByOrder(oid1);
                    String description = invoice.getDescription();
                    String[] tokens = description.split(" : ");
                    String[] pinfo = new String[tokens.length - 1];
                    for (int j = 0; j < tokens.length - 1; j++) {
                        pinfo[j] = tokens[j + 1];
                    }

                    for (int j = 0; j < pinfo.length; j++) {

                        String[] content = pinfo[j].split(" ; ");
                        String priceStr = content[3];
                        String discountStr = content[2];
                        Double price = Double.parseDouble(priceStr);
                        Double discount = Double.parseDouble(discountStr);
                        Integer number = hasProducts.get(j).getNumberOfProduct();

                        price = price * (1 - discount / 100);
                        totalPrice += number * (price);
                    }

                    totalPrice = (double) Math.round(totalPrice * 100) / 100;
                    prices.add(totalPrice);

                }

                request.setAttribute("prices", prices);
                request.setAttribute("orders", orders);
                request.getRequestDispatcher("/Jsp/OrderHistory.jsp").forward(request, response);

                break;

            case "browseOrderDetail":
                //given orderID

                Integer oid1 = Integer.parseInt(request.getParameter("orderId"));

                Authenticated authToken1 = authBean.findSession(
                        (String) request.getSession().getAttribute("authToken"));
                String emailCustomer1 = authToken1.getEmail();

                //for each order, there can be products array and numbers of each products.
                Orders orders1 = orderBean.find(oid1);
                int scid = orders1.getScid();

                ArrayList<ShoppingcartHasProducts> arrayList = shoppingCartProductBean.find(scid);
                ArrayList<Products> arrayListProduct = new ArrayList<>();
                ArrayList<Integer> arrayListInteger = new ArrayList<>();
                for (ShoppingcartHasProducts s : arrayList) {
                    Products p = productBean.find(s.getShoppingcartHasProductsPK().getPid());
                    int number = s.getNumberOfProduct();
                    arrayListProduct.add(p);
                    arrayListInteger.add(number);
                }

                Invoices invoice = invoiceBean.findByOrder(oid1);
                String description = invoice.getDescription();
                String[] tokens = description.split(" : ");
                String[] pinfo = new String[tokens.length - 1];
                for (int i = 0; i < tokens.length - 1; i++) {
                    pinfo[i] = tokens[i + 1];
                }

                for (int i = 0; i < pinfo.length; i++) {

                    String[] content = pinfo[i].split(" ; ");
                    String priceStr = content[3];
                    String discountStr = content[2];
                    Double price = Double.parseDouble(priceStr);
                    Double discount = Double.parseDouble(discountStr);

                    arrayListProduct.get(i).setPrice(price);
                    arrayListProduct.get(i).setDiscount(discount);
                }

                //Invoice will be add to this array there...
                request.setAttribute("productList", arrayListProduct);
                request.setAttribute("numbers", arrayListInteger);
                request.getRequestDispatcher("/Jsp/OrderDetailPage.jsp").forward(request, response); //where??

                break;

            case "lookTickets":

                Authenticated authToken4 = authBean.findSession(
                        (String) request.getSession().getAttribute("authToken"));
                String emailCustomer4 = authToken4.getEmail();

                //for each order, if ticket exists show them.
                List<Shoppingcart> shoppingcarts2 = shoppingCartBean.findOrdered(emailCustomer4);
                ArrayList<Orders> orders2 = new ArrayList<>();

                for (int i = 0; i < shoppingcarts2.size(); i++) {
                    Orders order = orderBean.findByScid(shoppingcarts2.get(i).getScid());
                    if (order != null) {
                        orders2.add(order);
                    }
                }

                ArrayList<Tickets> openedTickets = new ArrayList<>();
                for (int i = 0; i < orders2.size(); i++) {
                    int oid3 = orders2.get(i).getOid();
                    List<Tickets> ticket = ticketBean.find(oid3);
                    if (!ticket.isEmpty()) {
                        openedTickets.addAll(ticket);
                    }
                }

                request.setAttribute("tickets", openedTickets);
                request.getRequestDispatcher("Jsp/TicketBrowsingPage.jsp").forward(request, response); //where??

                break;

            default:
                break;
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

}
