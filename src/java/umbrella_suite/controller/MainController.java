package umbrella_suite.controller;

import com.itextpdf.text.DocumentException;
import java.io.IOException;
import java.util.ArrayList;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import umbrella_suite.model.*;
import umbrella_suite.dao.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebServlet(name = "MainController", urlPatterns = {"/MainController"})
public class MainController extends HttpServlet {

    //EJBs will be added to there...
    @EJB
    AuthenticatedFacadeLocal authBean;
    @EJB
    ProductTypesFacadeLocal productTypesBean;
    @EJB
    ProductsFacadeLocal productBean;
    @EJB
    ShoppingcartFacadeLocal shoppingcartBean;
    @EJB
    OrdersFacadeLocal orderBean;
    @EJB
    ShoppingcartHasProductsFacadeLocal shoppingCartProductBean;
    @EJB
    TicketsFacadeLocal ticketBean;
    @EJB
    CustomersFacadeLocal customerBean;
    @EJB
    ShoppingcartFacadeLocal shoppingCartBean;
    @EJB
    InvoicesFacadeLocal invoiceBean;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getParameter("action");

        //Visitor issue can change this below code, they can be transfered to inside of cases
//                Authenticated authToken = authBean.findSession(
//                        (String) request.getSession().getAttribute("authToken"));
//                String emailCustomer = authToken.getEmail();
        switch (action) {

            case "browseProducts": //name has changed from selectProductType to browseProducts

                String departmentName = request.getParameter("type");

                ProductTypes productType = productTypesBean.find(departmentName);
                List<Products> products = productBean.findByType(departmentName);

                if (request.getSession().getAttribute("authToken") == null) {
                    request.getSession(true).setAttribute("userType", "visitor");
                }

                request.setAttribute("productType", productType);
                request.setAttribute("products", products);
                request.getRequestDispatcher("Jsp/ProductDisplayingPage.jsp").forward(request, response);

                break;

            case "addCart":
                //TODO: take the pid / find the product / add it to the shopping cart of the user / return it to Product Diplaying Page pls 
                Integer pid = Integer.parseInt(request.getParameter("productId"));

                Authenticated authToken4 = authBean.findSession((String) request.getSession().getAttribute("authToken"));

                String emailCustomer4 = authToken4.getEmail();

                int scid = shoppingcartBean.find(emailCustomer4);
                ShoppingcartHasProducts hasProducts = shoppingCartProductBean.find(scid, pid);
                if (hasProducts == null) {
                    ShoppingcartHasProducts hasProducts2 = shoppingCartProductBean.processProductShopping(scid, pid);
                    shoppingCartProductBean.create(hasProducts2);
                } else { //hasProducts != null
                    shoppingCartProductBean.incrementProductQuantity(hasProducts);
                }

                int numberOfProductOnSC = shoppingCartProductBean.countByScid(scid);
                request.getSession(true).setAttribute("cartCount", numberOfProductOnSC);

                String departmentName2 = request.getParameter("productTypeName");
                ProductTypes productType2 = productTypesBean.find(departmentName2);
                List<Products> products2 = productBean.findByType(departmentName2);

                request.setAttribute("productType", productType2);
                request.setAttribute("products", products2);
                request.getRequestDispatcher("Jsp/ProductDisplayingPage.jsp").forward(request, response);

                break;

            case "updateShoppingCart":
                
                Authenticated authToken8 = authBean.findSession((String) request.getSession().getAttribute("authToken"));
                String emailCustomer8 = authToken8.getEmail();
                int scid8 = shoppingcartBean.find(emailCustomer8);

                Integer pid31 = Integer.parseInt(request.getParameter("pid"));
                Integer number = Integer.parseInt(request.getParameter("number"));
                
                ShoppingcartHasProducts shp = shoppingCartProductBean.find(scid8, pid31);
                shp.setNumberOfProduct(number);
                shoppingCartProductBean.edit(shp);
                
            case "displayCart":
                
                Authenticated authToken5 = authBean.findSession((String) request.getSession().getAttribute("authToken"));
                String emailCustomer5 = authToken5.getEmail();
                int scid2 = shoppingcartBean.find(emailCustomer5);

                ArrayList<ShoppingcartHasProducts> productsInShop = shoppingCartProductBean.find(scid2);

                ArrayList<Integer> numbers = new ArrayList<>();
                ArrayList<Products> productList = new ArrayList<>();

                for (int i = 0; productsInShop != null && i < productsInShop.size(); i++) {
                    int pid2 = productsInShop.get(i).getShoppingcartHasProductsPK().getPid();
                    Products products1 = productBean.find(pid2);
                    numbers.add(productsInShop.get(i).getNumberOfProduct());
                    productList.add(products1);
                }

                request.setAttribute("numbers", numbers);
                request.setAttribute("productList", productList);
                request.getRequestDispatcher("Jsp/ShoppingCartPage.jsp").forward(request, response);

                break;

            case "removeProductFromCart":

                Authenticated authToken6 = authBean.findSession((String) request.getSession().getAttribute("authToken"));
                String emailCustomer6 = authToken6.getEmail();
                int scid3 = shoppingcartBean.find(emailCustomer6);

                int pid1 = Integer.parseInt(request.getParameter("productId"));

                ShoppingcartHasProducts hasProducts1 = shoppingCartProductBean.find(scid3, pid1);
                shoppingCartProductBean.remove(hasProducts1);

                ArrayList<ShoppingcartHasProducts> productsInShop1 = shoppingCartProductBean.find(scid3);

                ArrayList<Integer> numbers1 = new ArrayList<>();
                ArrayList<Products> productList1 = new ArrayList<>();

                for (int i = 0; productsInShop1 != null && i < productsInShop1.size(); i++) {
                    int pid2 = productsInShop1.get(i).getShoppingcartHasProductsPK().getPid();
                    Products products1 = productBean.find(pid2);
                    numbers1.add(productsInShop1.get(i).getNumberOfProduct());
                    productList1.add(products1);
                }

                int numberOfProductOnSC1 = shoppingCartProductBean.countByScid(scid3);
                request.getSession(true).setAttribute("cartCount", numberOfProductOnSC1);

                request.setAttribute("numbers", numbers1);
                request.setAttribute("productList", productList1);
                request.getRequestDispatcher("Jsp/ShoppingCartPage.jsp").forward(request, response);

                break;

            case "checkOut":

                Authenticated authToken7 = authBean.findSession((String) request.getSession().getAttribute("authToken"));
                String emailCustomer7 = authToken7.getEmail();
                int scid4 = shoppingcartBean.find(emailCustomer7);

                ArrayList<ShoppingcartHasProducts> hasProducts2 = shoppingCartProductBean.find(scid4);
                ArrayList<Integer> numbers2 = new ArrayList<>();
                ArrayList<Integer> productIds = new ArrayList<>();
                
                if(hasProducts2 == null){
                    String message = "You don't have any products in shopping cart!";
                    request.setAttribute("exception", message);
                    request.getRequestDispatcher("Jsp/Exception.jsp").forward(request, response);
                }
                
                for (int i = 0; i < hasProducts2.size(); i++) {
                    numbers2.add(hasProducts2.get(i).getNumberOfProduct());
                    productIds.add(hasProducts2.get(i).getShoppingcartHasProductsPK().getPid());
                }
                
                String result = productBean.controlProductQuantity(numbers2, productIds);
                if (result.equals("YES")) {
                    result = "Success!";

                    //shoppingCartUpdate, addNewShoppingCart to the user.
                    Shoppingcart shoppingcart1 = shoppingCartBean.processShoppingCart(emailCustomer7);
                    if (shoppingcart1 != null) {
                        shoppingCartBean.create(shoppingcart1);
                    }

                    int scid5 = shoppingcartBean.find(emailCustomer7);
                    int numberOfProductOnSC2 = shoppingCartProductBean.countByScid(scid5);
                    request.getSession(true).setAttribute("cartCount", numberOfProductOnSC2);

                    Orders newOrder = orderBean.processOrder(scid4);
                    if (newOrder != null) {
                        orderBean.create(newOrder);
                    }

                    String address = (String) request.getSession(true).getAttribute("address");

                    ArrayList<Products> products1 = new ArrayList<>();
                    for (int i = 0; i < productIds.size(); i++) {
                        Products p = productBean.find(productIds.get(i));
                        if (p != null) {
                            products1.add(p);
                        }
                    }

                    Customers c = customerBean.find(emailCustomer7);
                    Integer cid = c.getCid();
                    String firstName = c.getFirstName();
                    String lastName = c.getLastName();

                    StringBuilder builder = new StringBuilder();
                    builder.append(cid).append(" ; ");
                    builder.append(firstName).append(" ; ");
                    builder.append(lastName).append(" ; ");
                    builder.append(address).append(" : ");

                    for (int i = 0; i < numbers2.size(); i++) {
                        String pName = products1.get(i).getPname();
                        Integer quantity = numbers2.get(i);
                        Double discount = products1.get(i).getDiscount();
                        Double price = products1.get(i).getPrice();
                        builder.append(pName).append(" ; ").append(quantity).append(" ; ");
                        builder.append(discount).append(" ; ").append(price).append(" : ");
                    }

                    String description = builder.toString().trim();
                    description = description.substring(0, description.length() - 3);

                    Orders order = orderBean.findByScid(scid4);

                    //Set the order invoice with using order products...
                    Invoices invoice = invoiceBean.processInvoice(address, order.getOid(), description);

                    if (invoice != null) {
                        invoiceBean.create(invoice);
                    }
                    
                    String fileLoc =  "";
                    try {
                        Invoices invob = invoiceBean.findByOrder(order.getOid());
                        int iid = invob.getIid();
                        InvoicePdf inv = new InvoicePdf(iid, description);
                        fileLoc = inv.createPdf();
                    } catch (DocumentException ex) {
                        Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    //send invoice as pdf in an email
                    String from = "umbrella.suite@gmail.com";
                    String message= "Dear " + firstName + ",\nThank you for shopping!...\n\nUmbrella Suite Team.";
                    String subject = "Invoice";
                    String smtp = "smtp.gmail.com";
                    //String filename = "invoice_" + iid + "_" + firstName + "_" + lastName + ".pdf";
                    Mail mail = new Mail();
                    mail.setTo(emailCustomer7);
                    mail.setFrom(from);
                    mail.setSmtpServ(smtp);
                    mail.setSubject(subject);
                    mail.setMessage(message);
                    mail.setFilename(fileLoc);
                    int successful = mail.sendMail(1);
                    if(successful == 0)
                        System.err.println("Invoice is NOT sent to " + emailCustomer7);

                } else {
                    request.setAttribute("exception", result);
                    request.getRequestDispatcher("Jsp/Exception.jsp").forward(request, response);
                }

                request.setAttribute("numbers", new ArrayList<Integer>());
                request.setAttribute("productList", new ArrayList<Products>());
                request.setAttribute("message", result);
                request.getRequestDispatcher("Jsp/ShoppingCartPage.jsp").forward(request, response);
                
                
                
                break;

            default:
                break;
        }

    }

    /* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        processRequest(request, response);
    }

    /* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        processRequest(request, response);
    }

}
