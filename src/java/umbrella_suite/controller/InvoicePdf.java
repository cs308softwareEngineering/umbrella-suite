/*
 * This class is used for creating pdf version of invoice
 * createPdf() method, creates the pdf and returns the location 
 * of pdf as a string.
 * 
 * In order to create an object invoiceID must be provided.
 *
 * Example usage:
 * String fileLocation = new Invoice(inviceID).createPdf();
 *
 */
package umbrella_suite.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPTableEvent;
import com.itextpdf.text.pdf.PdfWriter;
import java.util.Arrays;
import javax.ejb.EJB;
import umbrella_suite.dao.InvoicesFacade;
import umbrella_suite.dao.InvoicesFacadeLocal;
import umbrella_suite.model.Invoices;

/**
 *
 * @author mbenlioglu
 */
public class InvoicePdf {

    public static final String DEST = "web/Resources/pdf/";
    public static final String LOGO = "web/Resources/Logo-thumbnail.png";

    private final int iid;
    private final String[] cinfo; // cid, firstName, lastName
    private final String[] pinfo;
    private final String fileLoc;
    private double subtotal = 0;
    private double discount = 0;
    
    public InvoicePdf(int iid, String description) {
        this.iid = iid;
        String[] tokens = description.split(" : ");
        cinfo = tokens[0].split(" ; ");
        pinfo = new String[tokens.length-1];
        for (int i = 0; i < tokens.length-1; i++) {
            pinfo[i] = tokens[i+1];
        }
        fileLoc = new StringBuilder(DEST).append("invoice_").append(iid).append("_")
                .append(cinfo[1]).append("_").append(cinfo[2]).append(".pdf").toString();
        File file = new File(fileLoc);
        file.getParentFile().mkdirs();
    }

    public String createPdf() throws DocumentException, IOException {
        Document document = new Document();
        Image logo = Image.getInstance(LOGO);
        PdfWriter.getInstance(document, new FileOutputStream(fileLoc));
        document.open();
        PdfPTable table = new PdfPTable(12);
        table.setWidthPercentage(100);

        Paragraph p;

        PdfPCell logoCell = new PdfPCell(logo, true);
        logoCell.setRowspan(2);
        logoCell.setBorder(Rectangle.NO_BORDER);
        table.addCell(logoCell);
        PdfPCell name = new PdfPCell();
        Chunk slogan = new Chunk("\nFuture of Business", new Font(FontFamily.HELVETICA, 8, Font.ITALIC));
        p = new Paragraph(16, "UMBRELLA SUITE");
        // cell elements
        name.addElement(p);
        p = new Paragraph(10, slogan);
        p.setIndentationLeft(20);
        name.addElement(p);
        // cell settings
        name.setRowspan(2);
        name.setColspan(3);
        name.setBorder(PdfPCell.NO_BORDER);
        table.addCell(name);

        PdfPCell empty = new PdfPCell();
        empty.setRowspan(2);
        empty.setColspan(5);
        empty.setBorder(PdfPCell.NO_BORDER);
        table.addCell(empty);
        PdfPCell title = new PdfPCell();
        Chunk ch = new Chunk("INVOICE", new Font(FontFamily.HELVETICA, 18, Font.BOLD, BaseColor.GRAY));
        // cell elements
        p = new Paragraph(16, ch);
        title.addElement(p);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy");
        ch = new Chunk("Date:   " + LocalDateTime.now().format(formatter),
                new Font(FontFamily.HELVETICA, 8));
        p = new Paragraph(20, ch);
        title.addElement(p);

        ch = new Chunk("Invoice #:  " + Integer.toString(iid), new Font(FontFamily.HELVETICA, 8));
        p = new Paragraph(10, ch);
        title.addElement(p);

        ch = new Chunk("Customer ID: " + cinfo[0], new Font(FontFamily.HELVETICA, 8));
        p = new Paragraph(10, ch);
        title.addElement(p);
        // cell settings
        title.setRowspan(2);
        title.setColspan(4);
        title.setBorder(PdfPCell.NO_BORDER);
        table.addCell(title);
        
        document.add(table);
        document.add(new Paragraph("\n\nBill to:", new Font(FontFamily.HELVETICA, 10, Font.BOLDITALIC)));
        BaseFont bf = BaseFont.createFont();
        p = new Paragraph(cinfo[1] + " " + cinfo[2], new Font(bf, 10, Font.ITALIC));
        p.setIndentationLeft(bf.getWidthPoint("Bill to:", 10));
        document.add(p);

        p = new Paragraph(cinfo[3], new Font(bf, 10, Font.ITALIC));
        p.setIndentationLeft(bf.getWidthPoint("Bill to:", 10));
        document.add(p);

        String[] header = new String[]{"Description", "Quantity", "Discount", "Price"};
        table = new PdfPTable(header.length);
        table.setHeaderRows(1);
        table.setWidths(new int[]{10, 2, 2, 3});
        table.setWidthPercentage(95);
        table.setSpacingBefore(10);
        table.setSplitLate(false);
        // draw table borders only
        table.setTableEvent(new PdfPTableEvent() {
            @Override
            public void tableLayout(PdfPTable t, float[][] widths, float[] heights, int headerRows, int rowStart,
                    PdfContentByte[] canvases) {
                float width[] = widths[0];
                float x1 = width[0];
                float x2 = width[width.length - 1];
                float y1 = heights[0];
                float y2 = heights[heights.length - 1];
                PdfContentByte cb = canvases[PdfPTable.LINECANVAS];
                cb.rectangle(x1, y1, x2 - x1, y2 - y1);
                cb.stroke();
                cb.resetRGBColorStroke();
            }
        });
        for (String columnHeader : header) {
            PdfPCell headerCell = new PdfPCell();
            headerCell.addElement(new Phrase(columnHeader, FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD)));
            headerCell.setHorizontalAlignment(Element.ALIGN_CENTER);
            headerCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            headerCell.setBorderColor(BaseColor.BLACK);
            headerCell.setBackgroundColor(BaseColor.LIGHT_GRAY);
            headerCell.setPadding(3);
            table.addCell(headerCell);
        }
        // populate table
        for (String product : pinfo) {
            PdfPCell cell;
            String[] content = product.split(" ; ");
            for (String text : content) {
                cell = new PdfPCell();
                cell.addElement(new Phrase(text, FontFactory.getFont(FontFactory.HELVETICA, 10, Font.NORMAL)));
                cell.setPadding(5);
                cell.setBorder(PdfPCell.RIGHT);
                table.addCell(cell);
            }
            subtotal += (double) Math.round(((Double.parseDouble(content[3]) * Integer.parseInt(content[1]))) * 100) /100; 
            discount += (double) Math.round(((Double.parseDouble(content[3]) * Integer.parseInt(content[1])) * Double.parseDouble(content[2]) / 100) * 100) / 100;
        }
        subtotal = (double) Math.round(subtotal * 100) / 100;
        discount = (double) Math.round(discount * 100) / 100;
        double finalTotal = (double) Math.round((subtotal-discount) * 100) /100;
        document.add(table);
        table = new PdfPTable(2);
        table.setHorizontalAlignment(Element.ALIGN_RIGHT);
        table.setWidthPercentage(30);
        table.setWidths(new int[]{2, 3});
        PdfPCell cell = new PdfPCell(new Phrase("Subtotal: ", FontFactory.getFont(FontFactory.HELVETICA, 10, Font.NORMAL)));
        cell.setBorder(PdfPCell.NO_BORDER);
        cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase("$ " + Double.toString(subtotal), FontFactory.getFont(FontFactory.HELVETICA, 10, Font.NORMAL)));
        cell.setBorder(PdfPCell.NO_BORDER);
        cell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase("Discount: ", FontFactory.getFont(FontFactory.HELVETICA, 10, Font.NORMAL)));
        cell.setBorder(PdfPCell.NO_BORDER);
        cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase("$ " + Double.toString(discount), FontFactory.getFont(FontFactory.HELVETICA, 10, Font.NORMAL)));
        cell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase("Total: ", FontFactory.getFont(FontFactory.HELVETICA, 10, Font.NORMAL)));
        cell.setBorder(PdfPCell.NO_BORDER);
        cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        table.addCell(cell);
        cell = new PdfPCell(new Phrase("$ " + Double.toString(finalTotal), FontFactory.getFont(FontFactory.HELVETICA, 10, Font.NORMAL)));
        cell.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        cell.setBorder(PdfPCell.NO_BORDER);
        table.addCell(cell);
        document.add(table);

        p = new Paragraph(25, "Thank You For Your Business!", FontFactory.getFont(FontFactory.HELVETICA, 12, Font.BOLDITALIC));
        p.setAlignment(Element.ALIGN_CENTER);
        document.add(p);

        document.close();
        return fileLoc;
    }

}
