<%-- 
    Document   : ShoppingCartPage
    Created on : May 2, 2016, 6:44:14 PM
    Author     : ozergence
--%>
<%@page import="java.util.ArrayList" %>
<%@page import="umbrella_suite.model.Products" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<head>
  <title>Umbrella</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>

<%@include file="Login.jsp"%>
 
    <% if(request.getAttribute("message") != null){ 
          if(request.getAttribute("message").equals("Success!")){
    %>
    <script>
        window.alert("Purchase Completed!");
    </script>
    <% }else {%>
         <script>
        window.alert("Purchase Failed!");
    </script>
  <%    }
    } %> 
  
  <%
      ArrayList<Integer> numberList=(ArrayList<Integer>)request.getAttribute("numbers");
      ArrayList<Products> productList = (ArrayList<Products>)request.getAttribute("productList");
  %>
  <div class="container-fluid">
    <div class="row-fluid">
        <div class="col-sm-9">
           
  <h2>Shopping Cart</h2>          
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Product Name</th>
        <th>Price</th>
        <th>Quantity</th>
      </tr>
    </thead>
    <tbody>
        <%
            String pName,imgSrc;
            Double price,discount, totalPrice = 0.0;
            Integer count,totalCount=0,id;
            for(Integer i=0;i<productList.size();i++){
                pName=productList.get(i).getPname();
                price = productList.get(i).getPrice() * (1 - productList.get(i).getDiscount()/100);
                price = (double) Math.round(price * 100) / 100;
                discount = productList.get(i).getDiscount();
                count = numberList.get(i);
                totalCount += count;
                totalPrice += price * count;
                id = productList.get(i).getPid();
                imgSrc = productList.get(i).getImage();
                totalPrice = (double) Math.round(totalPrice * 100) / 100;
        %>        
        <tr>
            <td>            
                <div class="col-sm-4">
                     <img src="<%=imgSrc%>" class="img-responsive centre-block" width="150" alt="dressing-header-image">
               </div>
                <div class ="col-sm-8">
                    <h5><%=pName%></h5>
                    <form class="form-horizontal" action="/umbrella_suite/MainController">
                        <input class="hidden" value="<%=id%>" name="productId">
                        <button type="submit" class="btn btn-link" value="removeProductFromCart" name="action"> Remove</button>
                    </form>
                </div>
            </td>
            <td>
                <%=price%> <i class="fa fa-try" aria-hidden="true"></i> (-%<%=discount%>)
            </td>
            <td>
               <form class="form-inline" action="/umbrella_suite/MainController" role="form"> 
                <div class="input-group spinner" id="<%=id%>">
                     <input type="text" class="form-control"  name="number" id="<%=i%>" value="<%=count%>">
                     <div class="input-group-btn-vertical">
                      <button class="btn btn-default"  id="<%=i%>"  type="button"><i class="fa fa-caret-up"></i></button>
                      <button class="btn btn-default"  id="<%=i%>"  type="button"><i class="fa fa-caret-down"></i></button>
                     </div>
                 </div>
                     <input type="hidden" name="pid"  value="<%=id%>"  >
                     <button class="btn btn-primary" name="action" value="updateShoppingCart">Update</button>   
               </form>  
            </td>
        </tr>
        <% } %>                        
    </tbody>
  </table>
        </div>
        
        <div class="col-sm-3">
            <br><br><br><br><br>
            <div class="panel panel-danger">
                 <div class="panel-heading">
                    <form class="form-horizontal" role="form">
                        <h5>Subtotal(<%=totalCount%>):<%=totalPrice%> <i class="fa fa-try" aria-hidden="true"></i></h5>
                        <button type = "button" class="btn btn-warning btn-block" data-toggle="modal" data-target="#CheckoutModel"> Proceed to Checkout </button>
                    </form>
                 </div>
            </div>
        </div>
</html>

<!-- Product Count Arranger Script -->
<script>
(function ($) {
  $('.spinner .btn:first-of-type').on('click', function() {
       var spin_id = $(this).attr("id");       
    $('.spinner input').eq(spin_id).val( parseInt($('.spinner input').eq(spin_id).val(), 10) + 1);
  });
  $('.spinner .btn:last-of-type').on('click', function() {
    var spin_id = $(this).attr("id");
    if( $('.spinner input').eq(spin_id).val() != 1){
        $('.spinner input').eq(spin_id).val( parseInt($('.spinner input').eq(spin_id).val(), 10) - 1);
    }
  });
})(jQuery);
</script>

<!-- Shopping Cart Style Sheet -->
<style>
    .spinner {
  width: 100px;
}
.spinner input {
  text-align: right;
}
.input-group-btn-vertical {
  position: relative;
  white-space: nowrap;
  width: 1%;
  vertical-align: middle;
  display: table-cell;
}
.input-group-btn-vertical > .btn {
  display: block;
  float: none;
  width: 100%;
  max-width: 100%;
  padding: 8px;
  margin-left: -1px;
  position: relative;
  border-radius: 0;
}
.input-group-btn-vertical > .btn:first-child {
  border-top-right-radius: 4px;
}
.input-group-btn-vertical > .btn:last-child {
  margin-top: -2px;
  border-bottom-right-radius: 4px;
}
.input-group-btn-vertical i{
  position: absolute;
  top: 0;
  left: 4px;
}
</style>

<!-- Checkout Model -->
<div id="CheckoutModel" tabindex="-1" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Checking Out</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="Checkout" method="post" action="/umbrella_suite/MainController" role="form">
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Address:</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="address" name="address" placeholder="${address}"disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="firstName">Credit Card Number:</label>
                        <div class="col-sm-9">          
                            <input type="text" class="form-control" id="creditCardNo" name="creditCardNo" placeholder="Enter Credit Card Number">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="lastName">Credit Card Security Code:</label>
                        <div class="col-sm-9">          
                            <input type="text" class="form-control" id="securityCode" name="securityCode" placeholder="Enter Security Code">
                        </div>
                    </div>
                    <div class="form-group">        
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-primary btn-block" value="checkOut" name="action" >Complete Purchase </button> 
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>