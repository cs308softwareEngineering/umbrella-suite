<%-- 
    Document   : TicketPage
    Created on : May 8, 2016, 6:47:26 PM
    Author     : ozergence
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList" %>
<%@page import="umbrella_suite.model.Tickets" %>
<%@page import="umbrella_suite.model.Customers" %>

<!DOCTYPE html>

<%@include file="Login.jsp"%>

<head>
  <title>Umbrella</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>

<div class="col-sm-1"></div>
<div class="col-sm-10">
<table class="table table-responsive">
    <thead>
      <tr>
        <th> Customer id </th>
        <th> Customer name</th>
        <th>Order id</th>
        <th>Problem</th>
        <th>Status</th>
        <th>Reply</th>
      </tr>
    </thead>     
    <tbody>
        <% List<Tickets> tickets = (List<Tickets>)request.getAttribute("tickets");
           ArrayList<Customers> customers  = (ArrayList<Customers>)request.getAttribute("customers1");
            String status,reply,problem,customerName;           
            Integer orderId,customerId,ticketId;
            
            for (int i = 0; i < tickets.size();i++){
                 customerId = customers.get(i).getCid();
                 customerName = customers.get(i).getFirstName() +" "+ customers.get(i).getLastName();
                 orderId = tickets.get(i).getOid();
                 ticketId = tickets.get(i).getTid();
                 if(tickets.get(i).getActive()){
                     status = "Active";
                 }else{
                     status = "Resolved";
                 }
                 problem = tickets.get(i).getProblem();
                 reply = tickets.get(i).getReply();
        %>  
        <tr>
            <td>
                 <%=customerId%>
            </td>
            <td>
                 <%=customerName%>
            </td>
            <td>
                <%=orderId%>
            </td>
            <td>
                <textarea class="form-control" rows="3" name="reply" style="resize: none" disabled><%=problem%></textarea>
            </td>
            <td>                 
                 <form class="form-horizontal" id="setDelieveredforOrder" method="post" action="/umbrella_suite/ManagerController" role="form">
                         <%=status%> 
                        <input type="hidden" name="ticketId" value="<%=ticketId%>"/>
                        <button type="submit" value = "changeStatus" name="action" class="btn btn-link"><i class="fa fa-check-square-o" aria-hidden="true"></i></button>
                    </form>
            </td>
            <td>
                <form class="form-inline" role="form" actio="/umbrella_suite/ManagerController">
                    <textarea class="form-control" rows="3" name="reply" style="resize: none"><%=reply%></textarea>
                    <input type="hidden" name="tid" value="<%=ticketId%>"/>
                    <button class="btn btn-primary" name="action" value="replyTicket">Respond</button>
                </form>
            </td> 
        </tr>
        <%}%>
</tbody>
</div>
