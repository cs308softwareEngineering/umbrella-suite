<%-- 
    Document   : ProductDisplayingPage
    Created on : Apr 29, 2016, 8:11:23 PM
    Author     : Gence Özer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="umbrella_suite.model.Products" %>
<%@page import="java.util.List" %>

<!DOCTYPE html>
<head>
  <title>Umbrella</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
   <link rel="stylesheet" href="/umbrella_suite/Resources/style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>

<%@include file="Login.jsp"%>

<style>
.panel-height{
    min-height: 200px;
    max-height: 200px;
    padding: 0 !important;
    overflow-y: hidden;
}

.no-padding{
    padding: 0 !important;
}	
</style>
<!-- 
<div class="container">
    <img src="/umbrella_suite/Resources/online-shopping-2.png" style="margin-left: auto; margin-right: auto; display: block" class="img-responsive centre-block"></img>    
</div>-->
<br><br>
<dıv class="container" style="padding-bottom: 15px">
<img src="/umbrella_suite/Resources/offersanddealHEADER.png" class="img-responsive centre-block" style="padding-bottom: 15px"></img> 
</div>
<div class="container-fluid">
    <div class="row-fluid">
        <!--
        <div class="col-sm-2">
            <div class="panel panel-danger">
                <div class="panel-body">
                     <img src="/umbrella_suite/Resources/offersanddealHEADER.png" class="img-responsive centre-block"></img>       
                </div>
             </div>
        </div>
        -->
        <div class="col-sm-12">
               
            <%  List<Products> productList = (List<Products>) request.getAttribute("products");
               for (Products product: productList){
                   String productName = product.getPname();
                   Double productPrice = product.getPrice();
                   int count = product.getCount();
                   int productId = product.getPid();
                   String productTypeName = product.getPtname();
                   String productImageSrc = product.getImage();
                   Double discount = product.getDiscount();
                   String descripton = product.getDescription();
                   productPrice = product.getPrice() * (1 - product.getDiscount()/100);
                   productPrice = (double) Math.round(productPrice * 100) / 100;
            %>

            <div class="col-sm-3">  
                  <div class="panel panel-danger no-padding">
                    <div class="panel-heading panel-height ">
                        <a class="open-DetailModal btn btn-block " data-toggle="modal" data-count = "<%=count%>" data-imagesrc="<%=productImageSrc%>" data-description="<%=descripton%>" data-price="<%=productPrice%>" data-name="<%=productName%>" data-id="<%=productId%>" data-type = "<%=productTypeName%>" href="#productDetailModal">
                             <img src="<%=productImageSrc%>" class="img-responsive centre-block no-padding" alt="dressing-header-image">  
                        </a>
                    </div>
                    <div class="panel-body"><% out.println(productName); %></div>
                    <div class="panel-footer"><% out.println(productPrice); %><i class="fa fa-try" aria-hidden="true"></i> <% if(discount > 0.0){ %> (%<%=discount%>)<%;}%></div>                   
                   </div>
            </div>
            <%}%>  
        </div>
    </div>
</div>
        
        
        <!-- Product Detail Model -->
<div id="productDetailModal" tabindex="-1" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <label class="control-label col-sm-6" name="productNameField" id="productNameField" for="productNameField">Araba</label>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                      <div class="row-fluid">
                         <div class="col-sm-6">       
                             <img name="img" id="img" src="" class="img-responsive centre-block" style="width:%100" alt="dressing-header-image">                                                                         
                         </div>   
                            <div class="col-sm-6">
                              <label class="control-label" name="productDescription" id="productDescription" value=""></label>
                              <label class="label label-danger label-lg">Eligibility:<i class="" id="status" aria-hidden="true"></i></label>                    
                              <label class="control-label" name="productPriceText" id="productPriceText" value=""></label><i class="fa fa-try fa-try-lg" aria-hidden="true"></i>
                              <% if(session.getAttribute("userType").equals("customer")){ %>
                                  <form class="form-horizontal" method="post" action = "/umbrella_suite/MainController" role="form">
                                      <input type="hidden" name="productId" id="productId" value="">
                                      <input type="hidden" name="productTypeName" id="productTypeName" value="">
                                      <button type="submit" class="btn btn-primary btn-block" value="addCart" name="action"> Add to Shopping Cart</button>
                                  </form>    
                              <% } %>  
                            </div>
                      </div>
                </div>
            </div>
        </div>
    </div>
</div>
                         
                                                  
<script>                   
     $(document).on("click", ".open-DetailModal", function () {
     
     var price = $(this).data('price');
     $(".modal-body #productPriceText").html(price);
     
     var count = $(this).data('count');

     var name = $(this).data('name');
     $(".modal-header #productNameField").html(name);
     
      var type = $(this).data('type');
     $(".modal-body #productTypeName").val(type);
      
      var description = $(this).data('description');
     $(".modal-body #productDescription").html(description);
      
     var img = $(this).data('imagesrc');
     $(".modal-body #img").attr("src", img);
      
       var id = $(this).data('id');
     $(".modal-body #productId").val(id);
     
     
     if(count>0){
         $(".modal-body #status").attr("class","fa fa-check fa-check-lg");
     }else{
         $(".modal-body #status").attr("class","fa fa-times fa-times-lg");
     }
});
</script>
