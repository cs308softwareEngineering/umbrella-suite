<%-- 
    Document   : InvoicesPage
    Created on : May 8, 2016, 7:30:24 PM
    Author     : ozergence
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList" %>
<%@page import="umbrella_suite.model.Invoices" %>
<%@page import="umbrella_suite.model.Customers" %>

<!DOCTYPE html>

<%@include file="Login.jsp"%>

<head>
  <title>Umbrella</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>

<div class="col-sm-1"></div>
<div class="col-sm-10">
<table class="table table-responsive">
    <thead>
      <tr>
        <th>Invoice id </th>
        <th>Order Id</th>
        <th>Customer name</th>
        <th>Description</th>
        <th>Address</th>
      </tr>
    </thead>     
    <tbody>
        <% List<Invoices> invoices = (List<Invoices>)request.getAttribute("invoices");
           ArrayList<Customers> customers = (ArrayList<Customers>)request.getAttribute("customers2");
            String address,description,customerName;           
            Integer orderId,invoiceId;
            
            for (int i = 0; i < invoices.size();i++){
                 invoiceId = invoices.get(i).getIid();
                 customerName = customers.get(i).getFirstName() +" "+ customers.get(i).getLastName();
                 orderId = invoices.get(i).getOid();
                 description = invoices.get(i).getDescription();
                 address = invoices.get(i).getAddress();
        %>  
        <tr>
            <td>
                 <%=invoiceId%>
            </td>
            <td>
                 <%=orderId%>
            </td>
            <td>
                <%=customerName%>
            </td>
            <td> 
                <textarea class="form-control" rows="3" name="reply" style="resize: none" disabled> <%=description%> </textarea>
            </td>
            <td>                 
                <%=address%> 
            </td>
        </tr>
        <%}%>
</tbody>
</div>
