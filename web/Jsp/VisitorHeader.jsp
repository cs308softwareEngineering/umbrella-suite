<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!-- Creating url for navbars -->
<c:url var="electronicsUrl" value="/MainController">
   <c:param name="action" value="browseProducts"/>
   <c:param name="type" value="Electronics"/>
</c:url>

<c:url var="dressingUrl" value="/MainController">
   <c:param name="action" value="browseProducts"/>
   <c:param name="type" value="Dressing"/>
</c:url>


<c:url var="musicUrl" value="/MainController">
   <c:param name="action" value="browseProducts"/>
   <c:param name="type" value="Music"/>
</c:url>


<!--  Customer navigation bar -->
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>                        
            </button>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <!-- Left Navigation Bar -->
            <ul class="nav navbar-nav navbar-form">
                <li><a href="/umbrella_suite/Jsp"><img src="/umbrella_suite/Resources/Logo-thumbnail.png" class="img-rounded" alt="Site Logo" width="32" height="32"></a></li>
                <li><a href="${electronicsUrl}">Electronics</a></li>
                <li><a href="${dressingUrl}">Dressing</a></li>
                <li><a href="${musicUrl}">Music</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li><a href="#"><span class="glyphicon glyphicon-user"></span> Visitor </a></li>
                <li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span> Cart</a></li>
                <li><button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#SignInModal">Sign In</button></li>
            </ul>
        </div>
    </div>
</nav>


<!-- SignInModal -->
<div id="SignInModal" tabindex="-1" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Sign In</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="login" method="post" action="/umbrella_suite/UserController" role="form">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="email">Email:</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="password">Password:</label>
                        <div class="col-sm-10">          
                            <input type="password" class="form-control" id="password" name="password" placeholder="Enter password">
                        </div>
                    </div>
                    <div class="form-group">        
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" value="login" name="action" class="btn btn-warning btn-block">Submit</button>
                        </div>
                    </div>
                    <div class="form-group">        
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#RegisterModal">Don't have an account? </button> 
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>   

<!-<!-- RegisterModal -->
<div id="RegisterModal" tabindex="-1" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Register</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="SignUp" method="post" action="/umbrella_suite/UserController" role="form">
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Email:</label>
                        <div class="col-sm-9">
                            <input type="email" class="form-control" id="email" name="email" placeholder="Enter email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="firstName">First Name:</label>
                        <div class="col-sm-9">          
                            <input type="text" class="form-control" id="firstName" name="firstName" placeholder="Enter First Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="lastName">Last Name:</label>
                        <div class="col-sm-9">          
                            <input type="text" class="form-control" id="lastName" name="lastName" placeholder="Enter Last Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="address">Address:</label>
                        <div class="col-sm-9">          
                            <input type="text" class="form-control" id="address" name="address" placeholder="Enter Address">
                        </div>
                    </div>
                    <div class="form-group">        
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-primary btn-block" value="signUp" name="action" data-toggle="modal" data-target="#RegisterModal">Register </button> 
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>