<!DOCTYPE html>
<html lang="en">
<head>
  <title>Umbrella</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>

<!-- Identifies the user and displays a nav bar accordingly -->
   <%@include file="Login.jsp"%>

  <br>
  <!--  Carousel that displays  opportunities -->
  <div id="myCarousel" class="carousel slide" data-ride="carousel">

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">

      <div class="item active">
        <img class="img-responsive center-block" src="/umbrella_suite/Resources/Logo.png" alt="Chania" width="Auto" height="251">
      </div>

      <div class="item">
        <img class="img-responsive center-block" src="/umbrella_suite/Resources/electronics-header-alt.jpg" alt="Chania" width="Auto" height="251">
      
      </div>
        
        <div class="item">
        <img class="img-responsive center-block" src="/umbrella_suite/Resources/header_shopping.jpg" alt="Chania" width="Auto" height="251">
      
      </div>
        
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

  <!--  Cells of the product type contains link to the product pages  -->
<div class="container">  
<br><br>  
  <div class="row">
    <div class="col-sm-4">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <form class="form-horizontal" id="browseProducts" method="post" action="/umbrella_suite/MainController" role="form">
                    <input type="hidden" value="Electronics" name="type"> 
                    <button type="submit" class="btn btn-link btn-sm" value="browseProducts" name="action">Browse Electronics</button>
                </form>
            </div>
        <div class="panel-body"><img src="/umbrella_suite/Resources/electronic-header-image.jpg" class="img-responsive centre-block" alt="electronic-header-image"></div>
         <div class="panel-footer">All the electronic devices that you are looking for</div>		
      </div>
    </div>
    <div class="col-sm-4"> 
      <div class="panel panel-danger">
        <div class="panel-heading">
            <form class="form-horizontal" id="browseProducts" method="post" action="/umbrella_suite/MainController" role="form">
                    <input type="hidden" value="Dressing" name="type"> 
                    <button type="submit" class="btn btn-link btn-sm" value="browseProducts" name="action">Browse Dressing</button>
            </form>
        </div>
        <div class="panel-body"><img src="/umbrella_suite/Resources/dress-header-image.jpg" class="img-responsive centre-block" style="width:%100" alt="dressing-header-image"></div>
		<div class="panel-footer">All the dressing that you are looking for</div>
      </div>
    </div>
    <div class="col-sm-4"> 
      <div class="panel panel-danger">
        <div class="panel-heading">
             <form class="form-horizontal" id="browseProducts" method="post" action="/umbrella_suite/MainController" role="form">
                    <input type="hidden" value="Music" name="type"> 
                    <button type="submit" class="btn btn-link btn-sm" value="browseProducts" name="action">Browse Music</button>
            </form>
        </div>
        <div class="panel-body"><img src="/umbrella_suite/Resources/music-header-image.jpg" class="img-responsive centre-block" style="width:%100" alt="music-header-image"></div>
	<div class="panel-footer">All the music that you are looking for</div>
      </div>
    </div>
  </div>
</div><br>

<%@include file="Footer.jsp" %>

