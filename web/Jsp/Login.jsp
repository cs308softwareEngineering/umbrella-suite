<%-- 
    Document   : Login
    Created on : May 3, 2016, 10:17:38 PM
    Author     : ozergence
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
  <% if(session.getAttribute("authToken") == null){ %>
        <%@include file="VisitorHeader.jsp"%>
  <% } else if (session.getAttribute("userType").equals("salesmanager")){ %>
        <%@include file="SalesManagerHeader.jsp"%>
  <% } else if (session.getAttribute("userType").equals("productmanager")){ %>
        <%@include file="ProductManagerHeader.jsp"%>  
  <% } else if (session.getAttribute("userType").equals("technician")){ %>
        <%@include file="TechnicianHeader.jsp"%>  
          <% } else if (session.getAttribute("userType").equals("admin")){ %>
        <%@include file="AdminHeader.jsp"%>   
  <%  } else {%>
        <%@include file="CustomerHeader.jsp"%>
  <%  } %>
 