<%-- 
    Document   : OrderDetailPage
    Created on : May 4, 2016, 11:40:12 PM
    Author     : ozergence
--%>

<%@page import="java.util.ArrayList" %>
<%@page import="umbrella_suite.model.Products" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<head>
  <title>Umbrella</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>

<%@include file="Login.jsp"%>
 
  
  <%
      ArrayList<Integer> numberList=(ArrayList<Integer>)request.getAttribute("numbers");
      ArrayList<Products> productList = (ArrayList<Products>)request.getAttribute("productList");
  %>
  <div class="container-fluid">
    <div class="row-fluid">
        <div class="col-sm-9">
           
  <h2>Products</h2>          
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Product Name</th>
        <th>Price</th>
        <th>Quantity</th>
      </tr>
    </thead>
    <tbody>
        <%
            String pName,imgSrc;
            Double price,discount, totalPrice = 0.0;
            Integer count,totalCount=0,id;
            for(Integer i=0;i<productList.size();i++){
                pName=productList.get(i).getPname();
                price = productList.get(i).getPrice() * (1 - productList.get(i).getDiscount()/100);
                price = (double) Math.round(price * 100) / 100;
                discount = productList.get(i).getDiscount();
                count = numberList.get(i);
                totalCount += count;
                totalPrice += price * count;
                id = productList.get(i).getPid();
                imgSrc = productList.get(i).getImage();
                totalPrice = (double) Math.round(totalPrice * 100) / 100;
        %>        
        <tr>
            <td>            
                <div class="col-sm-4">
                     <img src="<%=imgSrc%>" class="img-responsive centre-block" width="150" alt="dressing-header-image">
               </div>
                <div class ="col-sm-8">
                    <h5><%=pName%></h5>
                </div>
            </td>
            <td>
                <%=price%> <i class="fa fa-try" aria-hidden="true"></i> (-%<%=discount%>)
            </td>
            <td>
                <%=count%>
            </td>
            
        </tr>
        <% } %>                        
    </tbody>
  </table>
        </div>
        <div class="col-sm-3">
            <br><br><br><br><br>
            <div class="panel panel-danger">
                 <div class="panel-body">
                        <h5>Subtotal(<%=totalCount%>):<%=totalPrice%> <i class="fa fa-try" aria-hidden="true"></i></h5>
                 </div>
            </div>
        </div>
    </div>
  </div>
</html>

