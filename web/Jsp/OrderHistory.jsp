<%-- 
    Document   : OrderHistory
    Created on : May 4, 2016, 10:34:21 PM
    Author     : ozergence
--%>

<%@page import="java.util.ArrayList" %>
<%@page import="umbrella_suite.model.Orders" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%@include file="Login.jsp"%>

<head>
  <title>Umbrella</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>

<div class="col-sm-1"></div>
<div class="col-sm-10">
<table class="table table-striped">
    <thead>
      <tr>
        <th>Order id</th>
        <th>Total Price</th>
        <th>Status</th>
        <th>Add Ticket</th>
        <th>View Products</th>
      </tr>
    </thead>     
    <tbody>
        <% ArrayList<Orders> orders = (ArrayList<Orders>)request.getAttribute("orders");
            ArrayList<Double> price = (ArrayList<Double>) request.getAttribute("prices");
            String status;
            Integer orderId;
            Double totalPrice;
            for (int i = 0; i < orders.size();i++){
                 orderId = orders.get(i).getOid();
                 status= orders.get(i).getOrderstatus();
                 totalPrice = price.get(i);
        %>  
        <tr>
            <td>
                <%=orderId%>
            </td>
            <td>
                <%=totalPrice%> <i class="fa fa-try" aria-hidden="true"></i>
            </td>
            <td>                 
                    <form class="form-horizontal" id="setDelieveredforOrder" method="post" action="/umbrella_suite/UserController" role="form">
                         <%=status%> 
                        <input type="hidden" name="orderId" value="<%=orderId%>"/>
                        <button type="submit" value = "setDelieveredforOrder" name="action" class="btn btn-link"><i class="fa fa-envelope" aria-hidden="true"></i></button>
                    </form>

            </td>
            <td>
               <button type="submit" class="btn open-ticket btn-link" data-toggle="modal" data-id="<%=orderId%>" href="#openTicket"><i class="fa fa-ticket" aria-hidden="true"></i>
            </td>
            <td>
                <form class="form-horizontal" id="browseOrderDetail" method="post" action="/umbrella_suite/UserController" role="form">
                    <input type="hidden" name="orderId" value="<%=orderId%>"/>
                    <button type="submit" class="btn btn-link" value="browseOrderDetail" name="action"><i class="fa fa-shopping-cart" aria-hidden="true"></i></button> 
                </form>
            </td>
                
        </tr>
        <%}%>
</tbody>
</div>

<!-<!-- Ticket Modal -->
<div id="openTicket" tabindex="-1" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Ticket</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="openTicket" method="post" action="/umbrella_suite/UserController" role="form">
                    <div class="form-group">
                        <label class="control-label col-sm-3" for="email">Problem:</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" row="5" id="problem" name="problem" placeholder="Enter your problem"></textarea>
                        </div>
                    </div>
                   <input type="hidden" id="orderIdField" name="orderIdField" value=""/>
                   <button class="btn btn-primary btn-block" type="submit" name="action" value="addTickets">Open Ticket</button>      
                </form>
            </div>
        </div>
    </div>
</div>

 <script>                   
     $(document).on("click", ".open-ticket", function () {
         
     var id = $(this).data('id');
     $(".modal-body #orderIdField").val(id);
     
});
</script>
             