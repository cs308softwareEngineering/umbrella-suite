<%-- 
    Document   : ManagerPage
    Created on : May 8, 2016, 8:09:58 PM
    Author     : ozergence
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@page import="java.util.List" %>
<%@page import="java.util.ArrayList" %>
<%@page import="umbrella_suite.model.Managers" %>

<!DOCTYPE html>

<%@include file="Login.jsp"%>

<head>
  <title>Umbrella</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>

<div class="col-sm-2">
    <br><br>
    <form class="form-vertical" action="/umbrella_suite/ManagerController" role="form">
        <label class="control-label" >Actions</label>
        <br>
        <button type="button" class="btn btn-block btn-primary" data-toggle="modal" data-target="#addManagerModal"> New Manager</button>
    </form>
</div>
<div class="col-sm-10">
<table class="table table-responsive">
    <thead>
      <tr>
        <th> Manager id </th>
        <th> Manager name</th>
        <th> Manager Email</th>
        <th> Role</th>
      </tr>
    </thead>     
    <tbody>
        <% List<Managers> managers = (List<Managers>)request.getAttribute("managers");
            String role,name,email;         
            Integer managerId;
            
            for (int i = 0; i < managers.size();i++){
                 role = managers.get(i).getType();
                 name = managers.get(i).getFirstName() +" "+ managers.get(i).getLastName();
                 email = managers.get(i).getEmail();
                 managerId = managers.get(i).getMid();
        %>  
        <tr>
            <td>
                 <%=managerId%>
            </td>
            <td>
                 <%=name%>
            </td>
            <td>
                <%=email%>
            </td>
            <td>
                <%=role%> 
            </td>
            <td>
                <form class="form-inline" role="form" actio="/umbrella_suite/ManagerController">
                    <input type="hidden" name="managerId" value="<%=managerId%>"/>
                    <button class="btn btn-danger" name="action" value="removeManager">Fire!!!</button>
                </form>
            </td> 
        </tr>
        <%}%>
</tbody>
</div>

<!-- Stock Set Modal -->
<div id="addManagerModal" tabindex="-1" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <label class="control-label col-sm-6" > Create new Manager</label>
            </div>
            <div class="modal-body">             
                  <form class="form-horizontal" id="setQuantity" method="post" action="/umbrella_suite/ManagerController"role="form">
                   <div class="form-group">
                      <label class="control-label col-sm-6" for="quantity">Email:</label>
                       <input type="email" name="managerEmail"/>
                   </div>
                      <div class="form-group">
                      <label class="control-label col-sm-6" for="quantity">First Name:</label>
                       <input type="text" name="managerFirstName"/>
                   </div>
                      <div class="form-group">
                      <label class="control-label col-sm-6" for="quantity">Last Name:</label>
                       <input type="text" name="managerLastName"/>
                   </div>
                      <div class="form-group">
                        <label class="control-label col-sm-6" for="quantity">Role:</label>
                         <input type="text" name="managerRole"/>
                   </div>
                    <button class="btn btn-primary btn-block" type="submit" value="addManager" name="action" id="addManager">Add Manager</button> 
                  </form>
            </div>
        </div>
    </div>
</div>
       