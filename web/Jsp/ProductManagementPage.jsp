<%-- 
    Document   : ProductManagementPage
    Created on : Apr 29, 2016, 10:23:58 PM
    Author     : Gence �zer
--%>
<%@page import="java.util.List" %>
<%@page import="umbrella_suite.model.Products" %>

<!DOCTYPE html>
<head>
  <title>Umbrella</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>

<%@include file="Login.jsp"%>
<div class="col-sm-1"></div>
<div class="col-sm-10">
<table class="table table-striped">
    <thead>
      <tr>
        <th>Product Name</th>
        <th>Price</th>
        <th>Discount</th>
        <th>Set Price/Discount</th>
      </tr>
    </thead>     
    <tbody>
        <% List<Products> products = (List<Products>)request.getAttribute("products");
            
            String productName;
            Integer productId;
            Double price,discount;
            for (Products p: products){
                 productName = p.getPname();
                 productId = p.getPid();
                 price = p.getPrice();
                 discount = p.getDiscount();
        %>  
        <tr>
            <input type="hidden" value="<%=productId%>"/>
            <td>
                <%=productName%>
            </td>
            <td>
                <%=price%> <i class="fa fa-try" aria-hidden="true"></i>
            </td>
            <td>
                %<%=discount%>
            </td>
            <td>
                <button type="submit" class="btn edit-modal btn-link" data-toggle="modal" data-id="<%=productId%>" data-name="<%=productName%>" data-price="<%=price%>" data-discount="<%=discount%>" href="#editModal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button> 
            </td>
                
        </tr>
        <%}%>
</tbody>
</div>
<div class="col-sm-1"></div>

     
        <!-- Price Discount Set Model -->
<div id="editModal" tabindex="-1" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <label class="control-label col-sm-6" name="productNameField" id="productNameField" for="productNameField"></label>
            </div>
            <div class="modal-body">             
                  <form class="form-horizontal" id="setPrice" method="post" action="/umbrella_suite/ManagerController"role="form">
                    <input type="hidden" name="productIdField" id="productIdField" value=""/>
                   <div class="form-group">
                      <label class="control-label col-sm-6" for="firstName">New Price:</label>
                       <input type="text" name="productPriceField" id="productPriceField" value=""/>
                   </div>
                    <div class="form-group">
                       <label class="control-label col-sm-6" for="firstName">New Discount Rate:</label>
                       <input type="text" name="productDiscountField" id="productDiscountField" value=""/> 
                    </div>
                    <button class="btn btn-primary btn-block" type="submit" value="setPrice" name="action" id="setPrice">Change Prices !!!</button> 
            </div>
        </div>
    </div>
</div>
        
 <script>                   
     $(document).on("click", ".edit-modal", function () {
         
     var id = $(this).data('id');
     $(".modal-body #productIdField").val(id);
     
      var price = $(this).data('price');
     $(".modal-body #productPriceField").val(price);
     
      var discount = $(this).data('discount');
     $(".modal-body #productDiscountField").val(discount);
     
      var name = $(this).data('name');
     $(".modal-header #productNameField").val(name);

});
</script>
                        
